begin;

update abilities set potency = 70  where id = 1001881; -- Combust III
update abilities set potency = 75 where id = 1001895; -- Biolysis
update abilities set potency = 75 where id = 1002616; -- Eukrasian Dosis III
update abilities set potency = 140 where id = 1003883; -- Baneful Impaction
update abilities set potency = 30 where id = 1000248; -- Circle of Scorn
update abilities set potency = 25 where id = 1001201; -- Stormbite
update abilities set potency = 60 where id = 1001838; -- Bow Shock
update abilities set potency = 20 where id = 1001200; -- Caustic Bite
update abilities set potency = 50 where id = 1001228; -- Higanbana
update abilities set potency = 60 where id = 1001837; -- Sonic Break
update abilities set potency = 45 where id = 1002719; -- Chaotic Spring

-- pets
update abilities set potency = 160 where id = 17414; -- Gust Slash
update abilities set potency = 160 where id = 17417; -- Armor Crush
update abilities set potency = 160 where id = 17415; -- Aeolian Edge
update abilities set potency = 160 where id = 25879; -- Fleeting Raiju
update abilities set potency = 160 where id = 17413; -- Spinning Edge

commit;
