update abilities set potency = 420 where id = 7481; -- Gekko
update abilities set potency = 420 where id = 7482; -- Kasha
update abilities set potency = 340 where id = 7480; -- Yukikaze
update abilities set potency = 1020 where id = 36966; -- Tendo Setsugekka
update abilities set potency = 800 where id = 16481; -- Hissatsu: Senei
update abilities set potency = 1020 where id = 36968; -- Tendo Kaeshi Setsugekka
update abilities set potency = 100 where id = 7492; -- Hissatsu: Gyoten
update abilities set potency = 820 where id = 36964; -- Zanshin
update abilities set potency = 900 where id = 25781; -- Ogi Namikiri
update abilities set potency = 900 where id = 25782; -- Kaeshi: Namikiri
update abilities set potency = 200 where id = 7489; -- Higanbana
update abilities set potency = 200 where id = 1001228; -- Higanbana
update abilities set potency = 640 where id = 16487; -- Shoha
update abilities set potency = 250 where id = 7490; -- Hissatsu: Shinten
update abilities set potency = 230 where id = 36963; -- Gyofu
update abilities set potency = 300 where id = 7479; -- Shifu
update abilities set potency = 300 where id = 7478; -- Jinpu
update abilities set potency = 640 where id = 7487; -- Midare Setsugekka
update abilities set potency = 100 where id = 7493; -- Hissatsu: Yaten
update abilities set potency = 100 where id = 7486; -- Enpi
update abilities set potency = 640 where id = 16486; -- Kaeshi: Setsugekka
update abilities set potency = 580 where id = 2271; -- Suiton
update abilities set potency = 320 where id = 74; -- Dragon Kick
update abilities set potency = 300 where id = 2240; -- Spinning Edge
update abilities set potency = 400 where id = 3547; -- The Forbidden Chakra
update abilities set potency = 0 where id = 1001895; -- Biolysis
update abilities set potency = 0 where id = 1001881; -- Combust III
update abilities set potency = 460 where id = 36945; -- Leaping Opo
update abilities set potency = 400 where id = 2242; -- Gust Slash
update abilities set potency = 270 where id = 25871; -- Fall Malefic
update abilities set potency = 310 where id = 25865; -- Broil IV
update abilities set potency = 480 where id = 3563; -- Armor Crush
update abilities set potency = 300 where id = 36957; -- Dokumori
update abilities set potency = 100 where id = 167; -- Energy Drain (sch)
update abilities set potency = 600 where id = 25775; -- Phantom Kamaitachi
update abilities set potency = 900 where id = 36948; -- Elixir Burst
update abilities set potency = 600 where id = 36958; -- Kunai's Bane
update abilities set potency = 150 where id = 3566; -- Dream Within a Dream
update abilities set potency = 400 where id = 7444; -- Lord of Crowns
update abilities set potency = 310 where id = 7441; -- Stellar Explosion
update abilities set potency = 1300 where id = 16492; -- Hyosho Ranryu
update abilities set potency = 500 where id = 18873; -- Fuma Shuriken
update abilities set potency = 1200 where id = 36950; -- Fire's Reply
update abilities set potency = 740 where id = 18877; -- Raiton
update abilities set potency = 860 where id = 37029; -- Oracle
update abilities set potency = 580 where id = 18881; -- Suiton
update abilities set potency = 900 where id = 36949; -- Wind's Reply
update abilities set potency = 740 where id = 2267; -- Raiton
update abilities set potency = 700 where id = 25778; -- Fleeting Raiju
update abilities set potency = 700 where id = 25879; -- Fleeting Raiju
update abilities set potency = 1100 where id = 36961; -- Tenri Jindo
update abilities set potency = 700 where id = 36960; -- Zesho Meppo
update abilities set potency = 0 where id = 1003883; -- Baneful Impaction
update abilities set potency = 380 where id = 7402; -- Bhavacakra
update abilities set potency = 300 where id = 17413; -- Spinning Edge
update abilities set potency = 420 where id = 61; -- Twin Snakes
update abilities set potency = 400 where id = 17414; -- Gust Slash
update abilities set potency = 420 where id = 66; -- Demolish
update abilities set potency = 540 where id = 2255; -- Aeolian Edge
update abilities set potency = 540 where id = 36946; -- Rising Raptor
update abilities set potency = 520 where id = 36947; -- Pouncing Coeurl
update abilities set potency = 900 where id = 25768; -- Rising Phoenix
update abilities set potency = 540 where id = 17415; -- Aeolian Edge
update abilities set potency = 270 where id = 25874; -- Macrocosmos
update abilities set potency = 1500 where id = 25769; -- Phantom Rush
update abilities set potency = 1020 where id = 16476; -- Six-sided Star
update abilities set potency = 480 where id = 17417; -- Armor Crush
update abilities set potency = 220 where id = 17870; -- Ruin II
update abilities set potency = 230 where id = 75; -- True Thrust
update abilities set potency = 300 where id = 36955; -- Spiral Blow
update abilities set potency = 340 where id = 25772; -- Chaotic Spring
update abilities set potency = 280 where id = 3555; -- Geirskogul
update abilities set potency = 340 where id = 1002719; -- Chaotic Spring
update abilities set potency = 340 where id = 3556; -- Wheeling Thrust
update abilities set potency = 400 where id = 16478; -- High Jump
update abilities set potency = 500 where id = 96; -- Dragonfire Dive
update abilities set potency = 440 where id = 36952; -- Drakesbane
update abilities set potency = 360 where id = 7400; -- Nastrond
update abilities set potency = 320 where id = 16479; -- Raiden Thrust
update abilities set potency = 720 where id = 16480; -- Stardiver
update abilities set potency = 340 where id = 36954; -- Lance Barrage
update abilities set potency = 900 where id = 36956; -- Starcross
update abilities set potency = 440 where id = 25771; -- Heavens' Thrust
update abilities set potency = 550 where id = 36953; -- Rise of the Dragon
update abilities set potency = 340 where id = 3554; -- Fang and Claw
update abilities set potency = 200 where id = 7399; -- Mirage Dive
update abilities set potency = 440 where id = 25773; -- Wyrmwind Thrust
update abilities set potency = 300 where id = 1001837; -- Sonic Break
update abilities set potency = 150 where id = 1001838; -- Bow Shock
update abilities set potency = 300 where id = 16137; -- Keen Edge
update abilities set potency = 380 where id = 16139; -- Brutal Shell
update abilities set potency = 460 where id = 16145; -- Solid Barrel
update abilities set potency = 500 where id = 16146; -- Gnashing Fang
update abilities set potency = 240 where id = 16156; -- Jugular Rip
update abilities set potency = 150 where id = 16159; -- Bow Shock
update abilities set potency = 300 where id = 16153; -- Sonic Break
update abilities set potency = 800 where id = 16165; -- Blasting Zone
update abilities set potency = 1200 where id = 25760; -- Double Down
update abilities set potency = 560 where id = 16147; -- Savage Claw
update abilities set potency = 280 where id = 16157; -- Abdomen Tear
update abilities set potency = 620 where id = 16150; -- Wicked Talon
update abilities set potency = 320 where id = 16158; -- Eye Gouge
update abilities set potency = 800 where id = 36937; -- Reign of Beasts
update abilities set potency = 1000 where id = 36938; -- Noble Blood
update abilities set potency = 1200 where id = 36939; -- Lion Heart
update abilities set potency = 460 where id = 16162; -- Burst Strike
update abilities set potency = 220 where id = 25759; -- Hypervelocity
update abilities set potency = 100 where id = 7407; -- Stormbite
update abilities set potency = 260 where id = 3558; -- Empyreal Arrow
update abilities set potency = 100 where id = 1001201; -- Stormbite
update abilities set potency = 150 where id = 7406; -- Caustic Bite
update abilities set potency = 180 where id = 36975; -- Heartbreak Shot
update abilities set potency = 280 where id = 7409; -- Refulgent Arrow
update abilities set potency = 150 where id = 1001200; -- Caustic Bite
update abilities set potency = 220 where id = 16495; -- Burst Shot
update abilities set potency = 220 where id = 7404; -- Pitch Perfect
update abilities set potency = 400 where id = 3562; -- Sidewinder
update abilities set potency = 600 where id = 36976; -- Resonant Arrow
update abilities set potency = 900 where id = 36977; -- Radiant Encore
update abilities set potency = 100 where id = 3560; -- Iron Jaws
update abilities set potency = 600 where id = 16496; -- Apex Arrow
update abilities set potency = 600 where id = 25784; -- Blast Arrow
update abilities set potency = 120 where id = 2247; -- Throwing Dagger
update abilities set potency = 150 where id = 3624; -- Unmend
update abilities set potency = 500 where id = 7384; -- Holy Spirit
update abilities set potency = 1000 where id = 34688; -- Rainbow Drip
update abilities set potency = 1100 where id = 34670; -- Pom Muse
update abilities set potency = 220 where id = 9; -- Fast Blade
update abilities set potency = 300 where id = 3617; -- Hard Slash
update abilities set potency = 460 where id = 16470; -- Edge of Shadow
update abilities set potency = 330 where id = 15; -- Riot Blade
update abilities set potency = 380 where id = 3623; -- Syphon Strike
update abilities set potency = 460 where id = 3539; -- Royal Authority
update abilities set potency = 480 where id = 3632; -- Souleater
update abilities set potency = 560 where id = 34678; -- Hammer Stamp
update abilities set potency = 580 where id = 36921; -- Imperator
update abilities set potency = 1000 where id = 16459; -- Confiteor
update abilities set potency = 140 where id = 23; -- Circle of Scorn
update abilities set potency = 450 where id = 25747; -- Expiacion
update abilities set potency = 800 where id = 34653; -- Blizzard in Cyan
update abilities set potency = 1000 where id = 36932; -- Disesteem
update abilities set potency = 420 where id = 17904; -- Abyssal Drain (pet)
update abilities set potency = 760 where id = 25748; -- Blade of Faith
update abilities set potency = 140 where id = 1000248; -- Circle of Scorn
update abilities set potency = 150 where id = 16461; -- Intervene
update abilities set potency = 600 where id = 36928; -- Scarlet Delirium
update abilities set potency = 600 where id = 25757; -- Shadowbringer
update abilities set potency = 840 where id = 34654; -- Stone in Yellow
update abilities set potency = 880 where id = 25749; -- Blade of Truth
update abilities set potency = 700 where id = 36929; -- Comeuppance
update abilities set potency = 570 where id = 25881; -- Shadowbringer (pet)
update abilities set potency = 880 where id = 34655; -- Thunder in Magenta
update abilities set potency = 540 where id = 3643; -- Carve and Spit
update abilities set potency = 1000 where id = 25750; -- Blade of Valor
update abilities set potency = 800 where id = 36930; -- Torcleaver
update abilities set potency = 420 where id = 17908; -- Edge of Shadow (pet)
update abilities set potency = 1100 where id = 34671; -- Winged Muse
update abilities set potency = 880 where id = 34663; -- Comet in Black
update abilities set potency = 1000 where id = 36922; -- Blade of Honor
update abilities set potency = 500 where id = 25756; -- Salt and Darkness
update abilities set potency = 1300 where id = 34676; -- Mog of the Ages
update abilities set potency = 700 where id = 3538; -- Goring Blade
update abilities set potency = 1400 where id = 34681; -- Star Prism
update abilities set potency = 420 where id = 17909; -- Bloodspiller (pet)
update abilities set potency = 580 where id = 7392; -- Bloodspiller
update abilities set potency = 620 where id = 34679; -- Hammer Brush
update abilities set potency = 460 where id = 16460; -- Atonement
update abilities set potency = 620 where id = 36933; -- Disesteem (pet)
update abilities set potency = 500 where id = 36918; -- Supplication
update abilities set potency = 680 where id = 34680; -- Polishing Hammer
update abilities set potency = 540 where id = 36919; -- Sepulchre
update abilities set potency = 440 where id = 34650; -- Fire in Red
update abilities set potency = 1100 where id = 34672; -- Clawed Muse
update abilities set potency = 480 where id = 34651; -- Aero in Green
update abilities set potency = 1100 where id = 34673; -- Fanged Muse
update abilities set potency = 520 where id = 34652; -- Water in Blue
update abilities set potency = 1400 where id = 34677; -- Retribution of the Madeen
update abilities set potency = 520 where id = 34662; -- Holy in White
update abilities set potency = 700 where id = 25777; -- Forked Raiju
update abilities set potency = 600 where id = 16500; -- Air Anchor
update abilities set potency = 160 where id = 36980; -- Checkmate
update abilities set potency = 330 where id = 25859; -- Glare III
update abilities set potency = 160 where id = 36979; -- Double Check
update abilities set potency = 200 where id = 34607; -- Reaving Fangs
update abilities set potency = 75 where id = 16532; -- Dia
update abilities set potency = 600 where id = 16498; -- Drill
update abilities set potency = 75 where id = 1001871; -- Dia
update abilities set potency = 300 where id = 34609; -- Swiftskin's Sting
update abilities set potency = 600 where id = 25788; -- Chain Saw
update abilities set potency = 500 where id = 34620; -- Vicewinder
update abilities set potency = 600 where id = 36981; -- Excavator
update abilities set potency = 620 where id = 34621; -- Hunter's Coil
update abilities set potency = 170 where id = 34636; -- Twinfang Bite
update abilities set potency = 170 where id = 34637; -- Twinblood Bite
update abilities set potency = 900 where id = 36982; -- Full Metal Field
update abilities set potency = 640 where id = 37009; -- Glare IV
update abilities set potency = 400 where id = 3571; -- Assize
update abilities set potency = 620 where id = 34622; -- Swiftskin's Coil
update abilities set potency = 220 where id = 36978; -- Blazing Shot
update abilities set potency = 750 where id = 34626; -- Reawaken
update abilities set potency = 240 where id = 16504; -- Arm Punch
update abilities set potency = 680 where id = 34627; -- First Generation
update abilities set potency = 280 where id = 34640; -- First Legacy
update abilities set potency = 680 where id = 34628; -- Second Generation
update abilities set potency = 280 where id = 34641; -- Second Legacy
update abilities set potency = 680 where id = 34629; -- Third Generation
update abilities set potency = 280 where id = 34642; -- Third Legacy
update abilities set potency = 680 where id = 34630; -- Fourth Generation
update abilities set potency = 280 where id = 34643; -- Fourth Legacy
update abilities set potency = 680 where id = 16503; -- Pile Bunker
update abilities set potency = 1150 where id = 34631; -- Ouroboros
update abilities set potency = 500 where id = 34612; -- Hindsting Strike
update abilities set potency = 780 where id = 25787; -- Crowned Collider
update abilities set potency = 220 where id = 7411; -- Heated Split Shot
update abilities set potency = 280 where id = 34634; -- Death Rattle
update abilities set potency = 680 where id = 34633; -- Uncoiled Fury
update abilities set potency = 170 where id = 34644; -- Uncoiled Twinfang
update abilities set potency = 320 where id = 7412; -- Heated Slug Shot
update abilities set potency = 170 where id = 34645; -- Uncoiled Twinblood
update abilities set potency = 300 where id = 34606; -- Steel Fangs
update abilities set potency = 400 where id = 7413; -- Heated Clean Shot
update abilities set potency = 150 where id = 16143; -- Lightning Shot
update abilities set potency = 300 where id = 34608; -- Hunter's Sting
update abilities set potency = 500 where id = 34611; -- Flanksbane Fang
update abilities set potency = 500 where id = 34613; -- Hindsbane Fang
update abilities set potency = 500 where id = 34610; -- Flanksting Strike
update abilities set potency = 480 where id = 17206; -- Roller Dash
update abilities set potency = 1320 where id = 16535; -- Afflatus Misery
update abilities set potency = 200 where id = 34632; -- Writhing Snap
update abilities set potency = 150 where id = 46; -- Tomahawk
update abilities set potency = 220 where id = 31; -- Heavy Swing
update abilities set potency = 340 where id = 37; -- Maim
update abilities set potency = 480 where id = 45; -- Storm's Eye
update abilities set potency = 660 where id = 16465; -- Inner Chaos
update abilities set potency = 580 where id = 3549; -- Fell Cleave
update abilities set potency = 400 where id = 7387; -- Upheaval
update abilities set potency = 150 where id = 7386; -- Onslaught
update abilities set potency = 700 where id = 25753; -- Primal Rend
update abilities set potency = 780 where id = 36925; -- Primal Ruination
update abilities set potency = 700 where id = 36924; -- Primal Wrath
update abilities set potency = 480 where id = 42; -- Storm's Path
update abilities set potency = 55 where id = 1003832; -- Damnation
update abilities set potency = 100 where id = 24; -- Shield Lob
update abilities set potency = 500 where id = 2265; -- Fuma Shuriken
update abilities set potency = 700 where id = 25878; -- Forked Raiju
update abilities set potency = 205 where id = 7440; -- Stellar Burst
update abilities set potency = 180 where id = 25866; -- Art of War II
update abilities set potency = 50 where id = 25776; -- Hollow Nozuchi
update abilities set potency = 100 where id = 16141; -- Demon Slice
update abilities set potency = 160 where id = 16149; -- Demon Slaughter
update abilities set potency = 300 where id = 24386; -- Harpe
update abilities set potency = 300 where id = 24378; -- Shadow of Death
update abilities set potency = 520 where id = 24380; -- Soul Slice
update abilities set potency = 520 where id = 24393; -- Gluttony
update abilities set potency = 820 where id = 36971; -- Executioner's Gallows
update abilities set potency = 820 where id = 36970; -- Executioner's Gibbet
update abilities set potency = 1000 where id = 24385; -- Plentiful Harvest
update abilities set potency = 560 where id = 24396; -- Cross Reaping
update abilities set potency = 530 where id = 36969; -- Sacrificium
update abilities set potency = 560 where id = 24395; -- Void Reaping
update abilities set potency = 280 where id = 24399; -- Lemure's Slice
update abilities set potency = 1100 where id = 24398; -- Communio
update abilities set potency = 1300 where id = 36973; -- Perfectio
update abilities set potency = 440 where id = 24391; -- Unveiled Gallows
update abilities set potency = 620 where id = 24383; -- Gallows
update abilities set potency = 420 where id = 24373; -- Slice
update abilities set potency = 500 where id = 24374; -- Waxing Slice
update abilities set potency = 440 where id = 24390; -- Unveiled Gibbet
update abilities set potency = 620 where id = 24382; -- Gibbet
update abilities set potency = 600 where id = 24375; -- Infernal Slice
update abilities set potency = 800 where id = 24388; -- Harvest Moon
update abilities set potency = 500 where id = 18874; -- Fuma Shuriken
update abilities set potency = 580 where id = 18878; -- Hyoton
update abilities set potency = 240 where id = 18879; -- Huton
update abilities set potency = 850 where id = 16192; -- Double Standard Finish
update abilities set potency = 1300 where id = 16196; -- Quadruple Technical Finish
update abilities set potency = 600 where id = 25790; -- Tillana
update abilities set potency = 420 where id = 25791; -- Fan Dance IV
update abilities set potency = 200 where id = 16009; -- Fan Dance III
update abilities set potency = 1000 where id = 36985; -- Dance of the Dawn
update abilities set potency = 520 where id = 36983; -- Last Dance
update abilities set potency = 850 where id = 36984; -- Finishing Move
update abilities set potency = 520 where id = 16005; -- Saber Dance
update abilities set potency = 600 where id = 25792; -- Starfall Dance
update abilities set potency = 340 where id = 15992; -- Fountainfall
update abilities set potency = 280 where id = 15991; -- Reverse Cascade
update abilities set potency = 220 where id = 15989; -- Cascade
update abilities set potency = 280 where id = 15990; -- Fountain
update abilities set potency = 150 where id = 16007; -- Fan Dance
update abilities set potency = 360 where id = 24316; -- Toxikon II
update abilities set potency = 360 where id = 24312; -- Dosis III
update abilities set potency = 0 where id = 1002616; -- Eukrasian Dosis III
update abilities set potency = 600 where id = 24313; -- Phlegma III
update abilities set potency = 600 where id = 37033; -- Psyche
update abilities set potency = 360 where id = 24318; -- Pneuma
update abilities set potency = 120 where id = 34614; -- Steel Maw
update abilities set potency = 600 where id = 25765; -- Celestial Revolution
update abilities set potency = 360 where id = 3579; -- Ruin III
update abilities set potency = 620 where id = 36994; -- Umbral Impulse
update abilities set potency = 160 where id = 36993; -- Luxwave
update abilities set potency = 200 where id = 16508; -- Energy Drain (smn)
update abilities set potency = 1400 where id = 36999; -- Exodus
update abilities set potency = 700 where id = 36996; -- Sunflare
update abilities set potency = 600 where id = 36991; -- Searing Flash
update abilities set potency = 440 where id = 36990; -- Necrotize
update abilities set potency = 800 where id = 25854; -- Aerial Blast
update abilities set potency = 490 where id = 25837; -- Slipstream
update abilities set potency = 240 where id = 25825; -- Emerald Rite
update abilities set potency = 800 where id = 25852; -- Inferno
update abilities set potency = 540 where id = 25823; -- Ruby Rite
update abilities set potency = 490 where id = 25835; -- Crimson Cyclone
update abilities set potency = 490 where id = 25885; -- Crimson Strike
update abilities set potency = 340 where id = 25824; -- Topaz Rite
update abilities set potency = 160 where id = 25836; -- Mountain Buster
update abilities set potency = 800 where id = 25853; -- Earthen Fury
update abilities set potency = 490 where id = 7426; -- Ruin IV
update abilities set potency = 500 where id = 25820; -- Astral Impulse
update abilities set potency = 150 where id = 7428; -- Wyrmwave
update abilities set potency = 500 where id = 3582; -- Deathflare
update abilities set potency = 1300 where id = 7449; -- Akh Morn
update abilities set potency = 580 where id = 16514; -- Fountain of Fire
update abilities set potency = 150 where id = 16519; -- Scarlet Flame
update abilities set potency = 1300 where id = 16518; -- Revelation
update abilities set potency = 340 where id = 24389; -- Blood Stalk
update abilities set potency = 240 where id = 3641; -- Abyssal Drain
update abilities set potency = 240 where id = 7391; -- Quietus
update abilities set potency = 160 where id = 16469; -- Flood of Shadow
update abilities set potency = 300 where id = 7488; -- Tenka Goken
update abilities set potency = 120 where id = 34656; -- Fire II in Red
update abilities set potency = 120 where id = 3621; -- Unleash
update abilities set potency = 150 where id = 25860; -- Holy III
update abilities set potency = 350 where id = 18876; -- Katon
