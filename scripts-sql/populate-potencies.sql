-- to get the csv:
-- > .mode csv
-- > .output test.csv
-- > select distinct ability_id, name from abilities inner join events on abilities.id = events.ability_id;
-- > .output stdout

-- job buffs
-- drk: x1.1

-- notes:
-- combo actions always from combo
-- assume positionals always hit
-- dots are a single tick
-- holy spirit - only ever under divine might (not without, and not with req)
-- firaga - assume UI3 (this is an unreliable data point)
-- thundaga (non-dot) - assume proc (this is also unreliable data point)
-- thundaja (non-dot) - assume not proc (not enough samples to be useful anyway)
-- holy circle - assume not proc
-- pitch perfect, apex arrow - assume max stacks
-- arm punch, pile bunker, crowned collider, roller dash - i have no clue just max it out ig
-- enpi - assume proc (there are literally 5 samples it doesn't matter)
-- flare - assume unenhanced (it's only me in dsr)
-- xeno - 800 (buffed to 880 in 6.4)

begin;

update abilities set potency = 150 where id = 3624; -- Unmend
update abilities set potency = 250 where id = 25871; -- "Fall Malefic"
update abilities set potency = 295 where id = 25865; -- "Broil IV"
update abilities set potency = 450 where id = 7384; -- "Holy Spirit"
update abilities set potency = 500 where id = 2271; -- Suiton normal
update abilities set potency = 220 where id = 2240; -- "Spinning Edge" ninja
update abilities set potency = 200 where id = 9; -- "Fast Blade"
update abilities set potency = 260 where id = 152; -- "Fire III"
update abilities set potency = 170 where id = 3617; -- "Hard Slash"
update abilities set potency = 460 where id = 16470; -- "Edge of Shadow" drk
update abilities set potency = 55 where id = 1001881; -- "Combust III"
update abilities set potency = 70 where id = 1001895; -- Biolysis
update abilities set potency = 400 where id = 153; -- "Thunder III"
update abilities set potency = 320 where id = 2242; -- "Gust Slash" ninja
update abilities set potency = 300 where id = 15; -- "Riot Blade"
update abilities set potency = 260 where id = 3623; -- "Syphon Strike"
update abilities set potency = 150 where id = 2248; -- Mug
update abilities set potency = 320 where id = 7383; -- Requiescat
update abilities set potency = 440 where id = 2255; -- "Aeolian Edge" ninja
update abilities set potency = 160 where id = 17415; -- "Aeolian Edge" pet
update abilities set potency = 400 where id = 3539; -- "Royal Authority"
update abilities set potency = 35 where id = 1000163; -- "Thunder III"
update abilities set potency = 340 where id = 3632; -- Souleater
update abilities set potency = 310 where id = 3577; -- "Fire IV"
update abilities set potency = 400 where id = 2258; -- "Trick Attack"
update abilities set potency = 140 where id = 23; -- "Circle of Scorn"
update abilities set potency = 1300 where id = 16492; -- "Hyosho Ranryu"
update abilities set potency = 500 where id = 7392; -- Bloodspiller drk
update abilities set potency = 450 where id = 25747; -- Expiacion
update abilities set potency = 30 where id = 1000248; -- "Circle of Scorn"
update abilities set potency = 150 where id = 3566; -- "Dream Within a Dream"
update abilities set potency = 150 where id = 16461; -- Intervene
update abilities set potency = 100 where id = 167; -- "Energy Drain"
update abilities set potency = 600 where id = 25757; -- Shadowbringer drk
update abilities set potency = 700 where id = 3538; -- "Goring Blade"
update abilities set potency = 650 where id = 2267; -- Raiton normal
update abilities set potency = 450 where id = 18873; -- "Fuma Shuriken" normal
update abilities set potency = 920 where id = 16459; -- Confiteor
update abilities set potency = 650 where id = 18877; -- Raiton tcj
update abilities set potency = 340 where id = 16505; -- Despair
update abilities set potency = 500 where id = 25756; -- "Salt and Darkness"
update abilities set potency = 500 where id = 18881; -- Suiton tcj
update abilities set potency = 310 where id = 7441; -- "Stellar Explosion"
update abilities set potency = 510 where id = 3643; -- "Carve and Spit" drk
update abilities set potency = 720 where id = 25748; -- "Blade of Faith"
update abilities set potency = 560 where id = 25778; -- "Fleeting Raiju" ninja
update abilities set potency = 160 where id = 25879; -- "Fleeting Raiju" pet
update abilities set potency = 240 where id = 17904; -- "Abyssal Drain"
update abilities set potency = 350 where id = 7402; -- Bhavacakra
update abilities set potency = 150 where id = 3640; -- Plunge drk
update abilities set potency = 350 where id = 17905; -- Plunge pet
update abilities set potency = 820 where id = 25749; -- "Blade of Truth"
update abilities set potency = 500 where id = 25881; -- Shadowbringer pet
update abilities set potency = 920 where id = 25750; -- "Blade of Valor"
update abilities set potency = 600 where id = 25775; -- "Phantom Kamaitachi"
update abilities set potency = 260 where id = 154; -- "Blizzard III"
update abilities set potency = 350 where id = 17908; -- "Edge of Shadow" pet
update abilities set potency = 220 where id = 17870; -- "Ruin II"
update abilities set potency = 400 where id = 16460; -- Atonement
update abilities set potency = 310 where id = 3576; -- "Blizzard IV"
update abilities set potency = 500 where id = 25797; -- Paradox
update abilities set potency = 350 where id = 17909; -- Bloodspiller pet
update abilities set potency = 160 where id = 17413; -- "Spinning Edge" pet
update abilities set potency = 350 where id = 17915; -- "Carve and Spit" pet
update abilities set potency = 800 where id = 16507; -- Xenoglossy
update abilities set potency = 420 where id = 3563; -- "Armor Crush" ninja
update abilities set potency = 250 where id = 25874; -- Macrocosmos
update abilities set potency = 120 where id = 2247; -- "Throwing Dagger" ninja
update abilities set potency = 160 where id = 17414; -- "Gust Slash" pet
update abilities set potency = 200 where id = 25876; -- Huraijin ninja
update abilities set potency = 180 where id = 25866; -- "Art of War II"
update abilities set potency = 600 where id = 7422; -- Foul
update abilities set potency = 250 where id = 7444; -- "Lord of Crowns"
update abilities set potency = 130 where id = 25872; -- "Gravity II"
update abilities set potency = 180 where id = 141; -- Fire
update abilities set potency = 160 where id = 17417; -- "Armor Crush" pet
update abilities set potency = 205 where id = 7440; -- "Stellar Burst"
update abilities set potency = 350 where id = 2268; -- Hyoton
update abilities set potency = 100 where id = 156; -- Scathe
update abilities set potency = 140 where id = 25795; -- "High Blizzard II"
update abilities set potency = 140 where id = 25794; -- "High Fire II"
update abilities set potency = 380 where id = 7481; -- Gekko
update abilities set potency = 380 where id = 7482; -- Kasha
update abilities set potency = 300 where id = 7480; -- Yukikaze
update abilities set potency = 640 where id = 7487; -- "Midare Setsugekka"
update abilities set potency = 860 where id = 16481; -- "Hissatsu: Senei"
update abilities set potency = 640 where id = 16486; -- "Kaeshi: Setsugekka"
update abilities set potency = 100 where id = 7492; -- "Hissatsu: Gyoten"
update abilities set potency = 250 where id = 7490; -- "Hissatsu: Shinten"
update abilities set potency = 200 where id = 7489; -- Higanbana
update abilities set potency = 45 where id = 1001228; -- Higanbana
update abilities set potency = 860 where id = 25781; -- "Ogi Namikiri"
update abilities set potency = 560 where id = 16487; -- Shoha
update abilities set potency = 860 where id = 25782; -- "Kaeshi: Namikiri"
update abilities set potency = 200 where id = 7477; -- Hakaze
update abilities set potency = 280 where id = 7479; -- Shifu
update abilities set potency = 280 where id = 7478; -- Jinpu
update abilities set potency = 100 where id = 7493; -- "Hissatsu: Yaten"
update abilities set potency = 100 where id = 25780; -- Fuko
update abilities set potency = 120 where id = 7485; -- Oka
update abilities set potency = 500 where id = 7496; -- "Hissatsu: Guren"
update abilities set potency = 120 where id = 7484; -- Mangetsu
update abilities set potency = 50 where id = 7420; -- "Thunder IV"
update abilities set potency = 20 where id = 1001210; -- "Thunder IV"
update abilities set potency = 300 where id = 24378; -- "Shadow of Death"
update abilities set potency = 460 where id = 24380; -- "Soul Slice"
update abilities set potency = 520 where id = 24393; -- Gluttony
update abilities set potency = 520 where id = 24383; -- Gallows
update abilities set potency = 520 where id = 24382; -- Gibbet
update abilities set potency = 1000 where id = 24385; -- "Plentiful Harvest"
update abilities set potency = 520 where id = 24396; -- "Cross Reaping"
update abilities set potency = 520 where id = 24395; -- "Void Reaping"
update abilities set potency = 240 where id = 24399; -- "Lemure's Slice"
update abilities set potency = 1100 where id = 24398; -- Communio
update abilities set potency = 400 where id = 24391; -- "Unveiled Gallows"
update abilities set potency = 320 where id = 24373; -- Slice
update abilities set potency = 400 where id = 24374; -- "Waxing Slice"
update abilities set potency = 400 where id = 24390; -- "Unveiled Gibbet"
update abilities set potency = 500 where id = 24375; -- "Infernal Slice"
update abilities set potency = 600 where id = 24388; -- "Harvest Moon"
update abilities set potency = 120 where id = 3621; -- Unleash
update abilities set potency = 140 where id = 16468; -- "Stalwart Soul"
update abilities set potency = 140 where id = 24376; -- "Spinning Scythe"
update abilities set potency = 180 where id = 24377; -- "Nightmare Scythe"
update abilities set potency = 300 where id = 24386; -- Harpe
update abilities set potency = 560 where id = 25777; -- "Forked Raiju" ninja
update abilities set potency = 100 where id = 24; -- "Shield Lob"
update abilities set potency = 720 where id = 16192; -- "Double Standard Finish"
update abilities set potency = 100 where id = 24379; -- "Whorl of Death"
update abilities set potency = 1200 where id = 16196; -- "Quadruple Technical Finish"
update abilities set potency = 360 where id = 25790; -- Tillana
update abilities set potency = 600 where id = 25792; -- "Starfall Dance"
update abilities set potency = 300 where id = 25791; -- "Fan Dance IV"
update abilities set potency = 200 where id = 16009; -- "Fan Dance III"
update abilities set potency = 340 where id = 15992; -- Fountainfall
update abilities set potency = 280 where id = 15991; -- "Reverse Cascade"
update abilities set potency = 220 where id = 15989; -- Cascade
update abilities set potency = 480 where id = 16005; -- "Saber Dance"
update abilities set potency = 280 where id = 15990; -- Fountain
update abilities set potency = 150 where id = 16007; -- "Fan Dance"
update abilities set potency = 450 where id = 2265; -- "Fuma Shuriken" tcj
update abilities set potency = 160 where id = 25877; -- Huraijin pet
update abilities set potency = 720 where id = 16194; -- "Double Technical Finish"
update abilities set potency = 100 where id = 16008; -- "Fan Dance II"
update abilities set potency = 160 where id = 25878; -- "Forked Raiju" pet
update abilities set potency = 330 where id = 24312; -- "Dosis III"
update abilities set potency = 310 where id = 25859; -- "Glare III"
update abilities set potency = 65 where id = 16532; -- Dia
update abilities set potency = 65 where id = 1001871; -- Dia
update abilities set potency = 75 where id = 1002616; -- "Eukrasian Dosis III"
update abilities set potency = 600 where id = 24313; -- "Phlegma III"
update abilities set potency = 400 where id = 3571; -- Assize
update abilities set potency = 330 where id = 24316; -- "Toxikon II"
update abilities set potency = 330 where id = 24318; -- Pneuma
update abilities set potency = 100 where id = 16458; -- "Holy Circle"
update abilities set potency = 150 where id = 25860; -- "Holy III"
update abilities set potency = 1240 where id = 16535; -- "Afflatus Misery"
update abilities set potency = 320 where id = 74; -- "Dragon Kick"
update abilities set potency = 280 where id = 61; -- "Twin Snakes"
update abilities set potency = 130 where id = 66; -- Demolish
update abilities set potency = 70 where id = 1000246; -- Demolish
update abilities set potency = 340 where id = 3547; -- "the Forbidden Chakra"
update abilities set potency = 310 where id = 53; -- Bootshine
update abilities set potency = 600 where id = 3545; -- "Elixir Field"
update abilities set potency = 700 where id = 25768; -- "Rising Phoenix"
update abilities set potency = 300 where id = 54; -- "True Strike"
update abilities set potency = 310 where id = 56; -- "Snap Punch"
update abilities set potency = 1150 where id = 25769; -- "Phantom Rush"
update abilities set potency = 550 where id = 16476; -- "Six-sided Star"
update abilities set potency = 100 where id = 3559; -- "the Wanderer's Minuet"
update abilities set potency = 100 where id = 7407; -- Stormbite
update abilities set potency = 25 where id = 1001201; -- Stormbite
update abilities set potency = 150 where id = 7406; -- "Caustic Bite"
update abilities set potency = 110 where id = 110; -- Bloodletter
update abilities set potency = 240 where id = 3558; -- "Empyreal Arrow"
update abilities set potency = 20 where id = 1001200; -- "Caustic Bite"
update abilities set potency = 280 where id = 7409; -- "Refulgent Arrow"
update abilities set potency = 220 where id = 16495; -- "Burst Shot"
update abilities set potency = 320 where id = 3562; -- Sidewinder
update abilities set potency = 360 where id = 7404; -- "Pitch Perfect"
update abilities set potency = 100 where id = 3560; -- "Iron Jaws"
update abilities set potency = 100 where id = 114; -- "Mage's Ballad"
update abilities set potency = 500 where id = 16496; -- "Apex Arrow"
update abilities set potency = 600 where id = 25784; -- "Blast Arrow"
update abilities set potency = 100 where id = 116; -- "Army's Paeon"
update abilities set potency = 170 where id = 24315; -- "Dyskrasia II"
update abilities set potency = 150 where id = 16143; -- "Lightning Shot"
update abilities set potency = 200 where id = 16137; -- "Keen Edge"
update abilities set potency = 300 where id = 16139; -- "Brutal Shell"
update abilities set potency = 300 where id = 16153; -- "Sonic Break"
update abilities set potency = 60 where id = 1001837; -- "Sonic Break"
update abilities set potency = 720 where id = 16165; -- "Blasting Zone"
update abilities set potency = 150 where id = 16159; -- "Bow Shock"
update abilities set potency = 1200 where id = 25760; -- "Double Down"
update abilities set potency = 150 where id = 16154; -- "Rough Divide"
update abilities set potency = 60 where id = 1001838; -- "Bow Shock"
update abilities set potency = 380 where id = 16146; -- "Gnashing Fang"
update abilities set potency = 200 where id = 16156; -- "Jugular Rip"
update abilities set potency = 460 where id = 16147; -- "Savage Claw"
update abilities set potency = 240 where id = 16157; -- "Abdomen Tear"
update abilities set potency = 540 where id = 16150; -- "Wicked Talon"
update abilities set potency = 280 where id = 16158; -- "Eye Gouge"
update abilities set potency = 360 where id = 16145; -- "Solid Barrel"
update abilities set potency = 380 where id = 16162; -- "Burst Strike"
update abilities set potency = 180 where id = 25759; -- Hypervelocity
update abilities set potency = 300 where id = 16163; -- "Fated Circle"
update abilities set potency = 100 where id = 16141; -- "Demon Slice"
update abilities set potency = 160 where id = 16149; -- "Demon Slaughter"
update abilities set potency = 900 where id = 16195; -- "Triple Technical Finish"
update abilities set potency = 340 where id = 24389; -- "Blood Stalk"
update abilities set potency = 310 where id = 3579; -- "Ruin III"
update abilities set potency = 440 where id = 25820; -- "Astral Impulse"
update abilities set potency = 150 where id = 7428; -- Wyrmwave
update abilities set potency = 1300 where id = 7449; -- "Akh Morn"
update abilities set potency = 200 where id = 16508; -- "Energy Drain"
update abilities set potency = 500 where id = 3582; -- Deathflare
update abilities set potency = 340 where id = 181; -- Fester
update abilities set potency = 330 where id = 25824; -- "Topaz Rite"
update abilities set potency = 150 where id = 25836; -- "Mountain Buster"
update abilities set potency = 750 where id = 25853; -- "Earthen Fury"
update abilities set potency = 750 where id = 25852; -- Inferno
update abilities set potency = 510 where id = 25823; -- "Ruby Rite"
update abilities set potency = 430 where id = 25835; -- "Crimson Cyclone"
update abilities set potency = 430 where id = 25885; -- "Crimson Strike"
update abilities set potency = 750 where id = 25854; -- "Aerial Blast"
update abilities set potency = 430 where id = 25837; -- Slipstream
update abilities set potency = 230 where id = 25825; -- "Emerald Rite"
update abilities set potency = 430 where id = 7426; -- "Ruin IV"
update abilities set potency = 540 where id = 16514; -- "Fountain of Fire"
update abilities set potency = 150 where id = 16519; -- "Scarlet Flame"
update abilities set potency = 1300 where id = 16518; -- Revelation
update abilities set potency = 600 where id = 16500; -- "Air Anchor"
update abilities set potency = 130 where id = 2874; -- "Gauss Round"
update abilities set potency = 130 where id = 2890; -- Ricochet
update abilities set potency = 600 where id = 16498; -- Drill
update abilities set potency = 600 where id = 25788; -- "Chain Saw"
update abilities set potency = 200 where id = 7410; -- "Heat Blast"
update abilities set potency = 200 where id = 7411; -- "Heated Split Shot"
update abilities set potency = 300 where id = 7412; -- "Heated Slug Shot"
update abilities set potency = 380 where id = 7413; -- "Heated Clean Shot"
update abilities set potency = 240 where id = 16504; -- "Arm Punch"
update abilities set potency = 680 where id = 16503; -- "Pile Bunker"
update abilities set potency = 780 where id = 25787; -- "Crowned Collider"
update abilities set potency = 480 where id = 17206; -- "Roller Dash"
update abilities set potency = 160 where id = 7401; -- "Hellfrog Medium"
update abilities set potency = 100 where id = 2254; -- "Death Blossom"
update abilities set potency = 150 where id = 3578; -- Painflare
update abilities set potency = 180 where id = 25821; -- "Astral Flare"
update abilities set potency = 240 where id = 16515; -- "Brand of Purgatory"
update abilities set potency = 100 where id = 16510; -- "Energy Siphon"
update abilities set potency = 100 where id = 117; -- "Rain of Death"
update abilities set potency = 210 where id = 25832; -- "Ruby Catastrophe"
update abilities set potency = 450 where id = 25765; -- "Celestial Revolution"
update abilities set potency = 260 where id = 7486; -- Enpi
update abilities set potency = 160 where id = 17418; -- "Throwing Dagger" pet
update abilities set potency = 220 where id = 162; -- Flare
update abilities set potency = 180 where id = 142; -- Blizzard
update abilities set potency = 600 where id = 16491; -- "Goka Mekkyaku"
update abilities set potency = 130 where id = 16488; -- "Hakke Mujinsatsu"
update abilities set potency = 100 where id = 15993; -- Windmill
update abilities set potency = 120 where id = 159; -- Freeze
update abilities set potency = 100 where id = 7381; -- "Total Eclipse"
update abilities set potency = 170 where id = 16457; -- Prominence
update abilities set potency = 140 where id = 15995; -- "Rising Windmill"
update abilities set potency = 350 where id = 2266; -- Katon
update abilities set potency = 540 where id = 16191; -- "Single Standard Finish"
update abilities set potency = 140 where id = 15994; -- Bladeshower
update abilities set potency = 200 where id = 24397; -- "Grim Reaping"
update abilities set potency = 180 where id = 15996; -- Bloodshower
update abilities set potency = 110 where id = 25767; -- "Shadow of the Destroyer"
update abilities set potency = 120 where id = 16473; -- "Four-point Fury"
update abilities set potency = 130 where id = 70; -- Rockbreaker
update abilities set potency = 200 where id = 24384; -- Guillotine
update abilities set potency = 360 where id = 16003; -- "Standard Finish"
update abilities set potency = 300 where id = 7488; -- "Tenka Goken"
update abilities set potency = 380 where id = 25855; -- "Verthunder III"
update abilities set potency = 380 where id = 25856; -- "Veraero III"
update abilities set potency = 460 where id = 7517; -- Fleche
update abilities set potency = 280 where id = 7527; -- "Enchanted Riposte"
update abilities set potency = 380 where id = 7519; -- "Contre Sixte"
update abilities set potency = 340 where id = 7528; -- "Enchanted Zwerchhau"
update abilities set potency = 130 where id = 7506; -- Corps-a-corps
update abilities set potency = 500 where id = 7529; -- "Enchanted Redoublement"
update abilities set potency = 180 where id = 16527; -- Engagement
update abilities set potency = 600 where id = 7525; -- Verflare
update abilities set potency = 680 where id = 16530; -- Scorch
update abilities set potency = 750 where id = 25858; -- Resolution
update abilities set potency = 340 where id = 7511; -- Verstone
update abilities set potency = 340 where id = 7510; -- Verfire
update abilities set potency = 320 where id = 7524; -- "Jolt II"
update abilities set potency = 600 where id = 7526; -- Verholy
update abilities set potency = 230 where id = 7516; -- Redoublement
update abilities set potency = 140 where id = 16524; -- Verthunder II
update abilities set potency = 180 where id = 7515; -- Displacement
update abilities set potency = 140 where id = 16525; -- Veraero II
update abilities set potency = 170 where id = 16474; -- Enlightenment
update abilities set potency = 150 where id = 7512; -- Zwerchhau

commit;
