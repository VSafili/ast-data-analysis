begin;

-- Tanks: Tank mastery -20%
-- Warrior: Surging Tempest +10%
update abilities set standardized_potency = cast(round(potency * 0.8 * 1.1) as integer)
where potency is not null and job = 'Warrior';

update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 16465; -- Inner Chaos
update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 25753; -- Primal Rend
update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 36925; -- Primal Ruination

-- Dark Knight: Darkside +10%
update abilities set standardized_potency = cast(round(potency * 0.8 * 1.1) as integer)
where potency is not null and job = 'Dark Knight';

-- Pet is unaffected by Tank mastery
--update abilities set standardized_potency = potency * 1.1 where id = 17905; -- Plunge pet
update abilities set standardized_potency = cast(round(potency * 1.0) as integer) where id = 25881; -- Shadowbringer pet
update abilities set standardized_potency = cast(round(potency * 1.0) as integer) where id = 17908; -- "Edge of Shadow" pet
update abilities set standardized_potency = cast(round(potency * 1.0) as integer) where id = 17909; -- Bloodspiller pet
-- update abilities set standardized_potency = potency * 1.1 where id = 17915; -- "Carve and Spit" pet
update abilities set standardized_potency = cast(round(potency * 1.0) as integer) where id = 36933; -- Disesteem (pet)
update abilities set standardized_potency = cast(round(potency * 1.0) as integer) where id = 17904; -- Abyssal Drain (pet)

-- Paladin: nothing
update abilities set standardized_potency = cast(round(potency * 0.8) as integer)
where potency is not null and job = 'Paladin';

-- Gunbreaker: nothing
update abilities set standardized_potency = cast(round(potency * 0.8) as integer)
where potency is not null and job = 'Gunbreaker';


-- Healers: Maim and Mend 2 +30%
update abilities set standardized_potency = cast(round(potency * 1.3) as integer)
where potency is not null and job = 'White Mage';

update abilities set standardized_potency = cast(round(potency * 1.3) as integer)
where potency is not null and job = 'Astrologian';

update abilities set standardized_potency = cast(round(potency * 1.3) as integer)
where potency is not null and job = 'Scholar';

update abilities set standardized_potency = cast(round(potency * 1.3) as integer)
where potency is not null and job = 'Sage';


-- The melee buffs show up) as buffs so we won't include them here
-- Monk: nothing
update abilities set standardized_potency = cast(round(potency * 1.0) as integer)
where potency is not null and job = 'Monk';

-- Bootshine practically always crits
--update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 53; -- Bootshine
update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 36945; -- Leaping Opo

-- Dragoon: Power Surge (doesn't show up as buff)
update abilities set standardized_potency = cast(round(potency * 1.0) as integer)
where potency is not null and job = 'Dragoon';

-- Viper: Hunter's Instinct
update abilities set standardized_potency = cast(round(potency * 1.0) as integer)
where potency is not null and job = 'Viper';

-- Ninja: nothing
update abilities set standardized_potency = cast(round(potency * 1.0) as integer)
where potency is not null and job = 'Ninja';

-- Hyosho is always under kassatsu
update abilities set standardized_potency = cast(round(potency * 1.3) as integer) where id = 16492; -- "Hyosho Ranryu"

-- Samurai: Jinpu/Fugetsu +13%
update abilities set standardized_potency = cast(round(potency * 1.0) as integer)
where potency is not null and job = 'Samurai';

-- Handle guaranteed crit
update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 7487; -- "Midare Setsugekka"
update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 16486; -- "Kaeshi: Setsugekka"
update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 25781; -- "Ogi Namikiri"
update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 25782; -- "Kaeshi: Namikiri"
update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 36966; -- Tendo Setsugekka
update abilities set standardized_potency = cast(round(potency * 1.6) as integer) where id = 36968; -- Tendo Kaeshi Setsugekka

-- Reaper: Death's Design +10%
update abilities set standardized_potency = cast(round(potency * 1.0) as integer)
where potency is not null and job = 'Reaper';


-- Ranged: Increased Action Damage 2 +20%
update abilities set standardized_potency = cast(round(potency * 1.2) as integer)
where potency is not null and job = 'Bard';

update abilities set standardized_potency = cast(round(potency * 1.2) as integer)
where potency is not null and job = 'Machinist';

update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 36982; -- Full Metal Field

-- I think the queen automaton doesn't get increased action damage
update abilities set standardized_potency = potency where id = 16503; -- "Pile Bunker"
update abilities set standardized_potency = potency where id = 25787; -- "Crowned Collider"
update abilities set standardized_potency = potency where id = 16504; -- "Arm Punch"
update abilities set standardized_potency = potency where id = 17206; -- "Roller Dash"

-- Dancer: Increased Action Damage 2 + 20%
update abilities set standardized_potency = cast(round(potency * 1.2) as integer)
where potency is not null and job = 'Dancer';

-- Handle guaranteed cdh
--update abilities set standardized_potency = cast(round(potency * 1.2 * 1.6 * 1.25) as integer) where id = 25792; -- "Starfall Dance"
update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 25792; -- "Starfall Dance"


-- Caster: Maim and Mend 2 +30%
update abilities set standardized_potency = cast(round(potency * 1.3) as integer)
where potency is not null and job = 'Summoner';

update abilities set standardized_potency = cast(round(potency * 1.3) as integer)
where potency is not null and job = 'Red Mage';

update abilities set standardized_potency = cast(round(potency * 1.3) as integer)
where potency is not null and job = 'Pictomancer';

update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 34678; -- Hammer Stamp
update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 34679; -- Hammer Brush
update abilities set standardized_potency = cast(round(potency * 1.2 * (1.6 + 0.25)) as integer) where id = 34680; -- Polishing Hammer

-- Black Mage: base: Maim and Mend 2 +30%, Enochian +33% (as of 7.05)
-- We'll ignore HF2 and HB2
update abilities set standardized_potency = cast(round(potency * 1.3 * 1.33) as integer)
where potency is not null and job = 'Black Mage';

-- Fire is usually under AF2 (in AF1 para F1 line)
update abilities set standardized_potency = cast(round(potency * 1.3 * 1.21 * 1.6) as integer)
where potency is not null and name = 'Fire';

-- Fire 3 is usually under UI3 (we'll ignore F3P and accept this) as noisy data)
-- Blizzard 3 is usually under AF3
update abilities set standardized_potency = cast(round(potency * 1.3 * 1.21 * 0.7) as integer)
where potency is not null and (name = 'Fire III' or name = 'Blizzard III');

-- Fire 4 and Despair and Flare are usually under AF3
update abilities set standardized_potency = cast(round(potency * 1.3 * 1.21 * 1.8) as integer)
where potency is not null and (name = 'Fire IV' or name = 'Despair' or name = 'Flare' or name = 'Flare Star');

commit;
