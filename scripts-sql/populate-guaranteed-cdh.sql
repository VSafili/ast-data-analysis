begin;

-- cdh
update abilities set guaranteed_crit = 1 where id = 25792; -- "Starfall Dance"
update abilities set guaranteed_dh = 1 where id = 25792; -- "Starfall Dance"
update abilities set guaranteed_crit = 1 where id = 34678; -- Hammer Stamp
update abilities set guaranteed_dh = 1 where id = 34678; -- Hammer Stamp
update abilities set guaranteed_crit = 1 where id = 34679; -- Hammer Brush
update abilities set guaranteed_dh = 1 where id = 34679; -- Hammer Brush
update abilities set guaranteed_crit = 1 where id = 34680; -- Polishing Hammer
update abilities set guaranteed_dh = 1 where id = 34680; -- Polishing Hammer
update abilities set guaranteed_crit = 1 where id = 36982; -- Full Metal Field
update abilities set guaranteed_dh = 1 where id = 36982; -- Full Metal Field
update abilities set guaranteed_crit = 1 where id = 16465; -- Inner Chaos
update abilities set guaranteed_dh = 1 where id = 16465; -- Inner Chaos
update abilities set guaranteed_crit = 1 where id = 25753; -- Primal Rend
update abilities set guaranteed_dh = 1 where id = 25753; -- Primal Rend
update abilities set guaranteed_crit = 1 where id = 36925; -- Primal Ruination
update abilities set guaranteed_dh = 1 where id = 36925; -- Primal Ruination

-- crit
update abilities set guaranteed_crit = 1 where id = 7487; -- "Midare Setsugekka"
update abilities set guaranteed_crit = 1 where id = 16486; -- "Kaeshi: Setsugekka"
update abilities set guaranteed_crit = 1 where id = 25781; -- "Ogi Namikiri"
update abilities set guaranteed_crit = 1 where id = 25782; -- "Kaeshi: Namikiri"
update abilities set guaranteed_crit = 1 where id = 36945; -- Leaping Opo
update abilities set guaranteed_crit = 1 where id = 36966; -- Tendo Setsugekka
update abilities set guaranteed_crit = 1 where id = 36968; -- Tendo Kaeshi Setsugekka

commit;
