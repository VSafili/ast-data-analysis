#!/usr/bin/env python3

import argparse
from enum import Enum
import sqlite3
import sys
import yaml

JOBS = [
    'Warrior', 'Paladin', 'Dark Knight', 'Gunbreaker',
    'White Mage', 'Scholar', 'Astrologian', 'Sage',
    'Monk', 'Samurai', 'Dragoon', 'Reaper', 'Ninja',
    # Bard is weird because of Barrage so we'll just skip it
    # 'Bard',
    'Machinist', 'Dancer',
    'Summoner', 'Black Mage', 'Red Mage',
]

ZONES = { 45: 'Dragonsong\'s Reprise', 53: 'The Omega Protocol' }

ALWAYS_DH = [
    25792 # "Starfall Dance"
]

ALWAYS_CRIT = [
    25792, # "Starfall Dance"
    7487, # "Midare Setsugekka"
    16486, # "Kaeshi: Setsugekka"
    25781, # "Ogi Namikiri"
    25782, # "Kaeshi: Namikiri"
]

class Status(Enum):
    COMPLETE = 0
    CONTINUE = 1
    ABORT = 2

class Accumulator(object):
    # This data is the same as the data in Sequence
    def __init__(self, data):
        self.data = data
        self.total_count = sum(data.values())
        self.output = []
        self.reset()

    def reset(self):
        self.matches = dict.fromkeys(self.data, 0)
        self.count = 0
        self.events = []
        self.total_potency = 0
        self.total_damage = 0

    # event is tuple of: timestamp_local, event_id, ability_id, standardized_potency, damage, multiplier, crit_rate_buff, dh_rate_buff
    def _accumulate(self, event):
        ability_id = event[2]
        crit_rate_buff = event[6]
        dh_rate_buff = event[7]

        # Skip autos
        if ability_id == 7 or ability_id == 8:
            return Status.CONTINUE

        key = (ability_id, crit_rate_buff, dh_rate_buff)
        if key not in self.matches:
            return Status.ABORT

        if self.matches[key] + 1 > self.data[key]:
            return Status.ABORT

        self.matches[key] += 1
        self.count += 1
        self.events.append(event[1])
        self.total_potency += event[3]
        damage = event[4] / event[5]

        if ability_id in ALWAYS_DH and dh_rate_buff > 0:
            damage /= 1 + (dh_rate_buff / 100)

        if ability_id in ALWAYS_CRIT and crit_rate_buff > 0:
            damage /= 1 + (crit_rate_buff / 100)

        self.total_damage += int(damage)

        if self.matches[key] < self.data[key]:
            return Status.CONTINUE

        complete = True
        for key in self.data.keys():
            if self.matches[key] != self.data[key]:
                complete = False

        return Status.COMPLETE if complete else Status.CONTINUE

    def accumulate(self, event):
        ret = self._accumulate(event)
        if ret == Status.ABORT:
            if self.count > 0:
                self.reset()
        if ret == Status.COMPLETE:
            self.output.append({'events': self.events, 'potency': self.total_potency, 'damage': self.total_damage})
            self.reset()

    def write(self, conn, seq_id):
        cur = conn.cursor()
        for entry in self.output:
            try:
                cur.execute('begin')
                cur.execute('''insert into sequence_instances (first_event_id, seq_id, damage_std, standardized_potency)
                               values (?, ?, ?, ?)''', (min(entry['events']), seq_id, entry['damage'], entry['potency']))
                seq_inst_id = cur.lastrowid
                for event_id in entry['events']:
                    cur.execute('''insert into sequence_instance_events
                                   (seq_instance_id, seq_info_id, event_id)
                                   values (?, ?, ?)''', (seq_inst_id, seq_id, event_id))
                cur.execute('commit')
            except:
                cur.execute('rollback')

# Ignore autos
class Sequence(object):
    def __init__(self, seq):
        self.name = seq['name']
        self.job = seq['job']
        self.zone = seq['zone']
        self.valid = True

        # Map from entry to count (number of instances needed for the sequence)
        # Entry is tuple of ability id, crit rate buff, dh rate buff
        self.data = {}

        if self.job not in JOBS:
            print(f'WARNING: Invalid job {self.job}, skipping sequence {self.name}')
            self.valid = False

        if self.zone not in ZONES:
            print(f'WARNING: Invalid zone {self.zone}, skipping sequence {self.name}')
            self.valid = False

        for entry in seq['info']:
            if isinstance(entry, list):
                if len(entry) == 1:
                    d = ( entry[0], 0, 0 )
                elif len(entry) == 2:
                    d = ( entry[0], entry[1], 0 )
                else:
                    d = ( entry[0], entry[1], entry[2] )
            else:
                d = ( entry, 0, 0 )

            if d not in self.data:
                self.data[d] = 0
            self.data[d] += 1

        self.accumulator = Accumulator(self.data)

    # event is tuple of: timestamp_local, event_id, ability_id, standardized_potency, damage, multiplier, crit_rate_buff, dh_rate_buff
    def count(self, event):
        self.accumulator.accumulate(event)

    def write(self, conn):
        cur = conn.cursor()
        try:
            cur.execute('begin')
            cur.execute('insert into sequence_infos (name) values (?)', (self.name,))
            seq_id = cur.lastrowid
            for key in self.data.keys():
                cur.execute('''insert into sequence_info_abilities (seq_id, ability_id, crit_rate_buff, dh_rate_buff, count)
                               values (?, ?, ?, ?, ?)''', (seq_id, key[0], key[1], key[2], self.data[key]))
            cur.execute('commit')
            self.accumulator.write(conn, seq_id)
        except:
            cur.execute('rollback')

# Members are tuples where the first index is the key
def merge_lists(list1, list2):
    if len(list1) == 0:
        return list2
    if len(list2) == 0:
        return list1

    ret = []
    while len(list1) > 0 and len(list2) > 0:
        if len(list1) == 0:
            ret.append(list2.pop(0))
            continue
        if len(list2) == 0:
            ret.append(list1.pop(0))
            continue

        if list1[0][0] <= list2[0][0]:
            ret.append(list1.pop(0))
        elif list1[0][0] > list2[0][0]:
            ret.append(list2.pop(0))

    return ret

def run_query(conn, query):
    cur = conn.cursor()
    cur.execute(query)
    return [tup[0] for tup in cur.fetchall()]

def get_reports(conn, zone, job):
    cur = conn.cursor()
    query = '''select distinct report_id
               from events inner join abilities on events.ability_id = abilities.id
               where events.zone_id = ? and abilities.job = ?;'''
    cur.execute(query, (zone, job))
    return [tup[0] for tup in cur.fetchall()]

def get_fights(conn, report_id):
    cur = conn.cursor()
    query = 'select distinct fight_id from fights where report_id = ?;'
    cur.execute(query, (report_id,))
    return [tup[0] for tup in cur.fetchall()]

def get_events(conn, report_id, fight_id, job, zone_id):
    queries = [
        # Non-dot damage events (excluding multi-hit events, including Dream Within a Dream and aoe)
        '''select timestamp_local, events.id, ability_id, standardized_potency, damage, multiplier, crit_rate_buff, dh_rate_buff
           from events inner join abilities on events.ability_id = abilities.id
           where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is null and ability_id != 3566
           group by packet_id having count(*) = 1;''',

        # Multi-hit non-dot damage events (aoe) (exclude Dream Within a Dream) - select the maximum
        '''select min(timestamp_local), events.id, ability_id, standardized_potency, max(damage), multiplier, crit_rate_buff, dh_rate_buff
           from events inner join abilities on events.ability_id = abilities.id
           where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is null and ability_id != 3566
           group by packet_id having count(*) > 1;''',

        # Aggregated dot damage events
        '''select min(timestamp_local), events.id, ability_id, standardized_potency, sum(damage), multiplier, crit_rate_buff, dh_rate_buff
           from events inner join abilities on events.ability_id = abilities.id
           where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is not null
           group by dot_info;'''
    ]

    if job == 'Ninja':
        queries += [
            # Multi-hit non-dot damage events (Dream Within a Dream)
            '''select min(timestamp_local), events.id, ability_id, standardized_potency, sum(damage), multiplier, crit_rate_buff, dh_rate_buff
               from events inner join abilities on events.ability_id = abilities.id
               where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is null and ability_id = 3566
               group by packet_id having count(*) > 1;''',
        ]


    results = []
    cur = conn.cursor()
    for query in queries:
        cur.execute(query, (report_id, fight_id, job, zone_id))
        results.append(cur.fetchall())

    merged = []
    for result in results:
        merged = merge_lists(merged, result)

    return merged

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i', type=str, required=True,
                        help='File with list of sequences and ability IDs to aggregate')
    parser.add_argument('--database', '-d', type=str, required=True,
                        help='Database file from which to aggregate sequences and to which to save sequences')
    args = parser.parse_args(argv[1:])

    with open(args.input, 'r') as file:
        config = yaml.safe_load(file)

    conn = sqlite3.connect(args.database)

    sequences = [Sequence(entry) for entry in config] 
    seq_map = {}
    for seq in sequences:
        key = (seq.zone, seq.job)
        if key not in seq_map:
            seq_map[key] = []
        seq_map[key].append(seq)

    zones = set([sequence.zone for sequence in sequences])
    jobs = set([sequence.job for sequence in sequences])

    for zone in zones:
        for job in jobs:
            reports = get_reports(conn, zone, job)
            for report_id in reports:
                fights = get_fights(conn, report_id)
                for fight_id in fights:
                    events = get_events(conn, report_id, fight_id, job, zone)
                    for event in events:
                        for seq in seq_map[(zone, job)]:
                            seq.count(event)

    # We want to use transactions
    conn.isolation_level = None

    for seq in sequences:
        seq.write(conn)

    conn.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
