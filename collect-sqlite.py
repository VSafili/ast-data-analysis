#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# collect-sqlite.py - Script to gather character stats and damage events from a list of FFLogs reports

import argparse
import bisect
from datetime import datetime
from fflogsapi import FFLogsClient, GQLEnum
import math
import os
from pathlib import Path
import sqlite3
import sys
import yaml

JOBS = [
    'Warrior', 'Paladin', 'Dark Knight', 'Gunbreaker',
    'White Mage', 'Scholar', 'Astrologian', 'Sage',
    'Monk', 'Samurai', 'Dragoon', 'Reaper', 'Ninja',
    'Bard', 'Machinist', 'Dancer',
    'Summoner', 'Black Mage', 'Red Mage',
    'Pictomancer', 'Viper',
]

MELEE_JOBS = [
    'Warrior', 'Paladin', 'Dark Knight', 'Gunbreaker',
    'Monk', 'Samurai', 'Dragoon', 'Reaper', 'Ninja',
    'Viper',
]

RATE_BUFFS = {
    'crit': {
        1001825: 20, # devilment
        1001221: 10, # chain
        1000786: 10, # litany
        1002216:  2, # minuet
    },

    'dh': {
        1001825: 20, # devilment
        1000141: 20, # battle voice
        1002218:  3, # paeon
    }
}

KNOWN_ENEMIES = {
    11810: 'Spear of the Fury',
    12601: 'Adelphel',
    12602: 'Grinnaux',
    12603: 'Charibert',
    12604: 'Thordan',
    12605: 'Niddhog (Estinien)',
    12609: 'Left eye',
    12610: 'Right eye',
    12611: 'Thordan (Alternate)',
    12612: 'Niddhog',
    12613: 'Hraesvelgr',
    12616: 'Thordan (Dragonking)',
    13920: 'Comet (Dragonsong)',
    15708: 'Omega',
    15712: 'Omega M',
    15713: 'Omega F',
    15717: 'Final Omega',
    15720: 'Golden Omega',
    15725: 'Alpha Omega',
    15726: 'Cosmo Meteor',
    15727: 'Cosmo Comet',

    16938: 'Honey B. Lovely',
    16941: 'Honey B. Lovely',
    17193: 'Black Cat',
    17094: 'Brute Bomber',
    17322: 'Wicked Thunder (Part 1)',
    17326: 'Wicked Thunder (Part 2)',

    17819: 'Fatebreaker',
    17823: 'Ursurper of Frost (P2)',
    17827: 'Crystal of Light',
    17828: 'Crystal of Darkness',
    17829: 'Ice Veil',
    17831: 'Oracle of Darkness (P3)',
    17833: 'Ursurper of Frost (P4)',
    17835: 'Oracle of Darkness (P4)',
    17839: 'Pandora',
}

# Get information on known game ids
def fetch_enemy_info(game_id):
    return KNOWN_ENEMIES[game_id] if game_id in KNOWN_ENEMIES else None

def process_characters(char_list, zone):
    return [Character(char, zone) for char in char_list]

class Resume(object):
    def __init__(self, path):
        self.path = path

        # Save the "next one that we need to work on"
        # Reports are 0-indexed, fights are 1-indexed
        self.report, self.fight = [0, 1]
        if os.path.isfile(self.path):
            with open(self.path, 'r') as file:
                self.report, self.fight = yaml.safe_load(file)

    def inc_report(self):
        self.report += 1
        self.fight = 1
        self.flush()

    def inc_fight(self):
        self.fight += 1
        self.flush()

    def flush(self):
        with open(self.path, 'w') as file:
            yaml.dump([self.report, self.fight], file)

    def clean(self):
        if os.path.isfile(self.path):
            os.remove(self.path)

class Stats(object):
    def __init__(self, stats):
        self.main = stats['main']
        self.crit_stat = stats['crit']
        self.det_stat = stats['det']
        self.dh_stat = stats['dh']

    def db_query_tuple(self):
        return (self.main, self.crit_stat, self.det_stat, self.dh_stat)

# Poor man's schema:
# - character:
#     name: Player Name
#     job: Astrologian
#     comment: This is a comment
#     match: name
#   stats:
#     main: 2940
#     crit: 2429
#     det: 1523
#     dh: 508
# - character:
#     name: Another Player
#     job: Dark Knight
#     match: job
#     comment: First Omega
#
# name, job, comment, and match are required
# If stats are specified then all four must be specified
#
# The value of the match field must be "name", "job", or "both"
# - "both" is recommended
# - "job" is recommended when working with anonymous logs. The job name must be
#   properly capitalized and spaced. It can also be used when the same person
#   uses the same job on different characters. Note, however, that these will
#   be attributed to the same character in the database.
# - "name" should be deprecated but still works (it was originally to be used
#   when one character has multiple jobs, but they would all link to the same
#   actor in the database so it's not recommended)
# All fields (except match) will be written into the database, and the match
# field will be used to match characters in the fight/log from which to extract
# information. The information will then be associated (via actor_id) to the
# character data as specified in the yaml file.
class Character(object):
    def __init__(self, obj, zone):
        char = obj['character']
        self.name = char['name']
        self.job = char['job']
        self.comment = char['comment'] if 'comment' in char else ''
        self.match = char['match']
        self.zone = zone
        self.stats = Stats(obj['stats']) if 'stats' in obj else None

        if not self.job in JOBS:
            raise Exception(f'Invalid job for "{self.name}": "{self.job}"')

        if self.match != 'name' and self.match != 'job' and self.match != 'both':
            raise Exception(f'Invalid match for "{self.name}": "{self.match}"')

    def db_query_tuple(self, t = 'insert'):
        half_tup = (self.name, self.job, self.zone, self.comment)

        if t == 'select':
            return half_tup

        if self.stats is None:
            return half_tup

        stats_tup = self.stats.db_query_tuple()
        return (*half_tup, *stats_tup)

    def write_to_db(self, conn):
        simple_query = '''
        insert into actors (name, job_id, zone_id, comment) values (?, ?, ?, ?);
        '''

        stats_query = '''
        insert into actors (name, job_id, zone_id, comment, main_stat,
                            crit_stat, det_stat, dh_stat)
            values (?, ?, ?, ?, ?, ?, ?, ?);
        '''

        cur = conn.cursor()
        cur.execute(simple_query if self.stats is None else stats_query, self.db_query_tuple())
        conn.commit()

    # Return list of report-specific ids
    def find(self, actors):
        if self.match == 'name':
            return [actor.id for actor in actors if actor.name == self.name]
        if self.match == 'job':
            return [actor.id for actor in actors if actor.job is not None and actor.job.name == self.job]
        if self.match == 'both':
            return [actor.id for actor in actors
                    if actor.name == self.name
                    and actor.job is not None and actor.job.name == self.job]
        raise Exception(f'Invalid match type "{self.match}"')

    def db_id(self, conn):
        query = 'select id from actors where name = ? and job_id = ? and zone_id = ? and comment = ?'
        cur = conn.cursor()
        cur.execute(query, self.db_query_tuple('select'))
        return cur.fetchone()[0]


class DamageEvent(object):
    # ev is either { 'damage': obj, 'calculateddamage': obj1 } or { 'damage': obj, 'tick': bool }
    # where obj{,1} is the json object result from the api call
    def __init__(self, fight, ev, actor_map, enemy_map, conn):
        obj = ev['damage']
        self.ts_global = datetime.fromtimestamp((fight.report.start_time() + obj['timestamp']) / 1000)
        self.ts_local = (obj['timestamp'] - fight.start_time()) / 1000
        self.ability_id = obj['abilityGameID']
        self.damage = obj['amount']
        self.multiplier = obj['multiplier']
        self.crit = True if obj['hitType'] == 2 else False
        self.dh = True if 'directHit' in obj and obj['directHit'] else False
        self.zone_id = fight.encounter().zone().id

        # This will be the case for pets
        if obj['sourceID'] not in actor_map:
            pet = [actor for actor in fight.report.actors() if actor.id == obj['sourceID']][0]
            self.actor_id = actor_map[pet.pet_owner.id]
        else:
            self.actor_id = actor_map[obj['sourceID']]

        try:
            self.target_id = enemy_map[obj['targetID']]
        except:
            print(fight.enemy_npcs())
            print(enemy_map)
            print(obj)
            print([e for e in fight.enemy_npcs() if e.id == obj['targetID']])
            print([e for e in fight.enemy_npcs() if e.id == obj['targetID']][0].actor.name != '')
            raise Exception
        self.report_id = fight.report.code
        self.fight_id = fight.id
        self.packet_id = obj['packetID']

        self.dot_tick = 'tick' in ev and ev['tick']

        calc_obj = obj if self.dot_tick else ev['calculateddamage']
        self.buff_ids = [int(buff) for buff in calc_obj['buffs'].split('.') if buff != ''] if 'buffs' in calc_obj else []

        self.crit_rate_buff = DamageEvent.calc_rate_buff('crit', self.buff_ids)
        self.dh_rate_buff = DamageEvent.calc_rate_buff('dh', self.buff_ids)

        if not self.dot_tick:
            self.dot_info = None
            self.dot_info_id = None
            self.dot_origin = None
            return

        self.dot_info = {
                'finalized_amount': obj['finalizedAmount'],
                'expected_amount': obj['expectedAmount'],
                'expected_crit_rate': obj['expectedCritRate'] / 1000,
                'actor_potency_ratio': obj['actorPotencyRatio'],
                'guess_amount': obj['guessAmount'],
                'direct_hit_percentage': obj['directHitPercentage'],
        }

        self.dot_info_id = self.get_from_db(conn, 'dot_infos', 'id')
        self.dot_origin = self.get_from_db(conn, 'events', 'id')


    def get_from_db(self, conn, table, key):
        query = f'select {key} from {table} where report_id = ? and fight_id = ? and packet_id = ?'
        cur = conn.cursor()
        cur.execute(query, (self.report_id, self.fight_id, self.packet_id))
        result = cur.fetchone()
        return None if result is None else result[0]


    def calc_rate_buff(key, buff_ids):
        if len(buff_ids) == 0:
            return 0

        return sum([RATE_BUFFS[key][buff_id] for buff_id in RATE_BUFFS[key] if buff_id in buff_ids])

    def write_dot_info_to_db(self, conn):
        if not self.dot_tick or self.dot_info_id is not None:
            return

        query = '''
        insert into dot_infos (finalized_amount, expected_amount, expected_crit_rate,
                               actor_potency_ratio, guess_amount, direct_hit_percentage,
                               report_id, fight_id, packet_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?);
        '''

        order = [
            'finalized_amount', 'expected_amount', 'expected_crit_rate',
            'actor_potency_ratio', 'guess_amount', 'direct_hit_percentage',
        ]
        params = tuple([self.dot_info[key] for key in order])
        match = tuple([self.report_id, self.fight_id, self.packet_id])
        cur = conn.cursor()
        cur.execute(query, (*params, *match))
        conn.commit()

        self.dot_info_id = self.get_from_db(conn, 'dot_infos', 'id')

    def to_tuple(self):
        return (
            self.ts_global.isoformat(),
            self.ts_local,
            self.ability_id,
            self.damage,
            self.multiplier,
            self.crit_rate_buff,
            self.dh_rate_buff,
            self.crit,
            self.dh,
            self.zone_id,
            self.actor_id,
            self.target_id,
            self.report_id,
            self.fight_id,
            self.dot_tick,
            self.dot_info_id,
            self.dot_origin,
            self.packet_id
        )

    def write_to_db(self, conn):
        self.write_dot_info_to_db(conn)

        query = '''
        insert into events (timestamp_global, timestamp_local, ability_id, damage, multiplier,
                            crit_rate_buff, dh_rate_buff, crit, dh, zone_id, actor_id,
                            target_id, report_id, fight_id, dot_tick, dot_info, dot_origin, packet_id)
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
        '''

        cur = conn.cursor()
        cur.execute(query, self.to_tuple())
        conn.commit()

        query = 'select id from events where report_id = ? and fight_id = ? and timestamp_global = ? and actor_id = ?'
        cur.execute(query, (self.report_id, self.fight_id, self.ts_global.isoformat(), self.actor_id))
        event_id = cur.fetchone()[0]

        for buff_id in self.buff_ids:
            query = '''
            insert into buffs (event_id, buff_id) values (?, ?);
            '''
            cur.execute(query, (event_id, buff_id))
            conn.commit()


def write_abilities(report, conn):
    add_ability_query = '''
    insert into abilities (id, name, type) values (?, ?, ?);
    '''

    cur = conn.cursor()
    for ability in report.abilities():
        cur.execute(add_ability_query, (ability.game_id, ability.name, ability.type))
    conn.commit()

def validate_zone(fight, zone_filter):
    # This could fail if there is no encounter/zone, such as field mobs
    try:
        zone_id = fight.encounter().zone().id
    except:
        return False
    return zone_id == zone_filter

# Return map from fight-specific actor id to primary key in the actors table
def match_actors(report, fight, characters, conn):
    # Match characters from our list to the list of actors in the report,
    # and further narrow down to the list of players in the fight
    actors = report.actors()
    players = fight.friendly_players()
    ret = {}
    for char in characters:
        matches = char.find(actors)
        matches = [match for match in matches if match in players]
        if len(matches) == 1:
            ret[matches[0]] = char.db_id(conn)
    return ret

# Return map from fight-specific enemy id to universal enemy game_id
# The latter is the key into the enemies table
def extract_and_write_enemies(fight, conn):
    add_enemy = '''
    insert into enemies (id, name, comment) values (?, ?, ?);
    '''

    # We also need to check for NPC because Cosmo Meteor counts as an NPC
    enemies = [enemy for enemy in fight.enemy_npcs()
               if enemy.actor.sub_type == 'Boss'
               or enemy.actor.sub_type == 'NPC']
    ret = {}
    cur = conn.cursor()
    for enemy in enemies:
        game_id = enemy.actor.game_id
        enemy_info = fetch_enemy_info(game_id)
        if enemy_info is None:
            continue

        cur.execute(add_enemy, (game_id, enemy.actor.name, enemy_info))
        ret[enemy.id] = game_id

    conn.commit()
    return ret

def insert_fight(report, fight, conn):
    query = '''
    insert into fights (report_id, fight_id, zone_id, timestamp) values (?, ?, ?, ?);
    '''
    date = datetime.fromtimestamp((report.start_time() + fight.start_time()) / 1000).isoformat()
    zone = fight.encounter().zone().id

    cur = conn.cursor()
    cur.execute(query, (report.code, fight.id, zone, date))
    conn.commit()

def process_events(fight, actor_id, actor_map, enemy_map, conn):
    damage_events = fight.events({'sourceID': actor_id, 'dataType': GQLEnum('DamageDone')})
    enemies = fight.enemy_npcs()
    # We need to get both the damage and the calculateddamage before we can
    # construct the DamageEvent instance
    # This is a map from packet id to map from target id to queue of events
    events_map = {}
    for event in damage_events:
        # This can happen if the enemy was immune and took zero damage
        if 'packetID' not in event:
            continue

        if not any(e.id == event['targetID'] for e in enemies):
            print(f'Unknown target id {event["targetID"]} in report {fight.report.code} fight {fight.id}; skipping event {event}')
            continue

        packet_id = event['packetID']
        # We can't skip calculateddamage because it contains the list of buffs for non-dots
        # Dot tick event can be constructed immediately as it contains the buff list
        if ('tick' in event and event['tick']):
            e = DamageEvent(fight, { 'damage': event, 'tick': True }, actor_map, enemy_map, conn)
            e.write_to_db(conn)

        # Match by both packet id and target id, as multi-target hits share the same packet id
        #
        # Add an extra case to filter for enemies in the enemy_map. We allow
        # enemies that are not in the map just in case there's a known enemy
        # that we haven't yet added to the known enemies list, in which case we
        # make sure it has a name (and thus isn't a dummy enemy like what
        # happens in the background of DSR.)
        elif packet_id in events_map and event['targetID'] in events_map[packet_id] and \
             (event['targetID'] in enemy_map or \
             [e for e in enemies if e.id == event['targetID']][0].actor.name != ''):
            queue = events_map[packet_id][event['targetID']]

            # We need to match differing types, that is 'calculateddamage' with 'damage'
            # The queue is necessary for skills like Dream Within a Dream, that
            # have three calculateddamage events first followed by three damage
            # events, all on the same target and with the same packet id
            if event['type'] == queue[0]['type']:
                queue.append(event)
                continue

            ev_in_q = queue.pop(0)
            ev_arg = { ev_in_q['type']: ev_in_q, event['type']: event }
            e = DamageEvent(fight, ev_arg, actor_map, enemy_map, conn)
            e.write_to_db(conn)
        else:
            if packet_id not in events_map:
                events_map[packet_id] = { event['targetID']: [ event ] }
                continue
            if event['targetID'] not in events_map[packet_id]:
                try:
                    events_map[packet_id][event['targetID']] = [ event ]
                except IndexError as e:
                    print(f'Unknown target id {event["targetID"]}')
                    raise e
                continue
            events_map[packet_id][event['targetID']].append(event)

def process_fight(fight, zone, characters, conn, resume):
    if not validate_zone(fight, zone):
        print(f'Skipping fight {fight.id}; invalid zone')
        return

    actor_map = match_actors(fight.report, fight, characters, conn)
    if len(actor_map) == 0:
        print(f'Skipping fight {fight.id}; no characters matched')
        return

    tracked_actors = [actor for actor in fight.report.actors() if actor.id in actor_map.keys()]

    print(f'Processing fight {fight.id}')
    insert_fight(fight.report, fight, conn)

    enemy_map = extract_and_write_enemies(fight, conn)

    for actor_id in actor_map:
        process_events(fight, actor_id, actor_map, enemy_map, conn)

def process_report(client, report_id, zone_filter, characters, conn, resume):
    report = client.get_report(report_id)

    # We could optimize these out based on resume but it's probably low enough
    # overhead that we can leave it as-is
    write_abilities(report, conn)

    # Fights are 1-indexed
    for fight_id in range(1, report.fight_count() + 1):
        if fight_id < resume.fight:
            continue
        fight = report.fight(fight_id)
        process_fight(fight, zone_filter, characters, conn, resume)
        resume.inc_fight()

def main(argv):
    config_file = '.config.yaml'
    if not os.path.isfile(config_file):
        print('Error: .config.yaml doesn\'t exist')
        return -1

    with open(config_file, 'r') as file:
        client_config = yaml.safe_load(file)

    if 'client_id' not in client_config or 'client_secret' not in client_config:
        print(f'{config_file} must have client_id field and client_secret field. See README.md for details.')
        return -1

    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i', type=str, required=True,
                        help='File with list of report IDs and character filters to process')
    parser.add_argument('--output', '-o', type=str, required=True,
                        help='Database file to output processed data to')
    parser.add_argument('--user-mode', '-u', action='store_true',
                        help='Run in user mode (required for private or archived logs)')
    args = parser.parse_args(argv[1:])

    client = FFLogsClient(client_config['client_id'], client_config['client_secret'],
                          mode='user' if args.user_mode else 'client')

    if args.user_mode:
        client.user_auth()

    with open(args.input, 'r') as file:
        config = yaml.safe_load(file)
        zone_filter = config['zone']
        reports = config['reports']
        characters = process_characters(config['characters'], zone_filter)

    resume = Resume('.collect-sqlite-progress.tmp')

    conn = sqlite3.connect(args.output)

    for char in characters:
        char.write_to_db(conn)

    # Reports are 0-indexed
    num_reports = len(reports)
    for i, report_id in enumerate(reports):
        if i < resume.report:
            continue
        print(f'Processing report {report_id}, progress: {i}/{num_reports} ({int(i * 100 / num_reports)}%)')
        process_report(client, report_id, zone_filter, characters, conn, resume)
        print(f'Done processing report {report_id} (index {i})')
        resume.inc_report()

    resume.clean()
    conn.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
