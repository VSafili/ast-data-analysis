#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# graph_damage_spread_aggregate_blom.py - Generate histograms for sequences of black mage lines

import lib

import argparse
from matplotlib import pyplot as plt
from pathlib import Path
import sys
import yaml

# These are my stats for DSR
job = 'Black Mage'
# We'll just say mind to avoid naming conflicts with, you know, the alternative...
mind = 2700
det = 1.076
master_stats_key = f'{mind}@0.083x1.433,0.305x1.25,{det}'

skill_dict = {
        'Blizzard': 'B1', 
        'Blizzard II': 'B2', 
        'Blizzard III': 'B3', 
        'Blizzard IV': 'B4', 
        'Despair': 'Desp', 
        'Fire': 'F1', 
        'Fire II': 'F2', 
        'Fire III': 'F3', 
        'Fire IV': 'F4', 
        'Flare': 'Flare', 
        'Foul': 'Foul', 
        'High Blizzard II': 'HB2', 
        'High Fire II': 'HF2', 
        'Paradox': 'Para', 
        'Scathe': 'Scathe', 
        'Thunder I': 'T1', 
        'Thunder II': 'T2', 
        'Thunder III': 'T3', 
        'Thunder IV': 'T4', 
        'Xenoglossy': 'Xeno', 
}

def match_skills(skills1, skills2):
    return sorted(skills1) == sorted(skills2)

def process_data(data, output_arr, skills, skip_rate_buffs):
    main_stat = data['stats']['main_stat']
    crit_rate = data['stats']['crit_rate']
    crit_mult = data['stats']['crit_mult']
    dh_rate = data['stats']['dh_rate']
    det_mult = data['stats']['det_mult']
    stats_key = f'{main_stat}@{crit_rate}x{crit_mult},{dh_rate}x1.25,{det_mult}'

    if data['stats']['job'] != job or stats_key != master_stats_key:
        return

    collector = {'skills': [], 'amounts': []}

    for event in data['events']:
        name = event['name']

        if name not in skill_dict or skill_dict[name] not in skills:
            continue

        if skip_rate_buffs and \
           (event['crit_rate'] != crit_rate or event['dh_rate'] != dh_rate):
            continue

        amount = int(event['amount'] / event['multiplier'])

        collector['skills'].append(skill_dict[name])
        collector['amounts'].append(amount)

        if match_skills(collector['skills'], skills):
            output_arr.append(sum(collector['amounts']))
            collector['skills'].pop(0)
            collector['amounts'].pop(0)

        if len(collector['skills']) == len(skills):
            collector['skills'].pop(0)
            collector['amounts'].pop(0)

def validate_skills(skills):
    invalid = []
    for skill in skills:
        if skill not in skill_dict.values():
            invalid.append(skill)
    return invalid

def main(argv):
    parser = lib.common_parser()
    parser.add_argument('--skip-rate-buffs', '-s', action='store_true',
                        required=False, dest='skip_rate_buffs',
                        help='Skip events that have crit or dh rate buffs')
    parser.add_argument('skills', nargs='+', type=str,
                        help='List of skills to aggregate (standard capitalized abbreviations) (unordered)')
    args = parser.parse_args(argv[1:])

    invalid = validate_skills(args.skills)
    if len(invalid) > 0:
        print(f'Invalid skill(s): {invalid}')
        return -1

    lib.create_path_if_not_exist(args.output)

    output_arr = []

    for data in lib.iterate_collection(args.input):
        process_data(data, output_arr, args.skills, args.skip_rate_buffs)

    seq = '-'.join(args.skills).lower()
    skip = '-skip' if args.skip_rate_buffs else ''

    with open(Path(args.output) / f'samples-{seq}{skip}.yaml', 'w') as outfile:
        yaml.dump({'samples': output_arr}, outfile)

    title = f'Damage spread: {seq} ({master_stats_key})'
    lib.draw_damage_histogram(output_arr, Path(args.output) / f'histogram-{seq}{skip}.png', title, 50)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
