#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# collect-raw.py - Script to download and save the raw data from a list of FFLogs reports

import argparse
from datetime import datetime
from fflogsapi import FFLogsClient, GQLEnum
import os
from pathlib import Path
import pickle
import sys
import yaml

id_dsr = 45
id_top = 53

def get_char_stats(fight, report, char_name):
    # There could be multiple instances of the character if for example they
    # participated in different encounters with different jobs in the same report
    chars = [actor for actor in report.actors() if actor.name == char_name]
    if len(chars) < 1:
        print('No character')
        return None

    for char_info in chars:
        stats = fight.events({'sourceID': char_info.id, 'dataType': GQLEnum('CombatantInfo')})
        if stats is None:
            print('No combatant info')
            return None
        if len(stats) > 0:
            break
    if len(stats) < 1:
        print('No combatant info')
        return None
    char_stats = stats[0]

    # Character didn't participate in combat
    if char_info.job is None:
        return None

    # Character was involved in combat but didn't do anything hence there's no data
    if 'attackMagicPotency' not in char_stats:
        return None

    # Just in case, block all other jobs
    if char_info.job.name != 'Black Mage' and char_info.job.name != 'Summoner' and \
       char_info.job.name != 'Astrologian' and char_info.job.name != 'Scholar':
        return None

    return [char_stats, char_info.job.name, char_info.id]

def filter_zone(fight):
    try:
        zone_id = fight.encounter().zone().id
    except:
        return False
    if zone_id != id_dsr and zone_id != id_top:
        return False

    return True

def write_to_file(out_dir, report_id, report, fight, events, stats, char_name, job):
    name = char_name.split(' ')[0].lower()
    date = datetime.fromtimestamp(report.start_time() / 1000).strftime('%Y%m%d')

    out_path = Path(out_dir) / f'{date}_{report_id}_{fight.id:02d}_{name}_{job}.pickle'
    obj = {'stats': stats, 'events': events,
           'abilities': report.abilities()}
    with open(out_path, 'wb') as outfile:
        pickle.dump(obj, outfile)

def process_fight(fight, report_id, report, char_name, abilities, out_dir):
    ret = get_char_stats(fight, report, char_name)
    if ret is None:
        return False
    stats, job, char_id = ret

    if not filter_zone(fight):
        return False

    damage_events = fight.events({'sourceID': char_id, 'dataType': GQLEnum('DamageDone')})
    write_to_file(out_dir, report_id, report, fight, damage_events, stats, char_name, job)
    return True

def process_report(client, report_id, char_name, out_dir):
    report = client.get_report(report_id)
    fights = [report.fight(i) for i in range(1, report.fight_count() + 1)]
    abilities = {e.game_id: e for e in report.abilities()}
    for fight in fights:
        print(f'Processing fight {fight.id} from report {report_id}...')
        ret = process_fight(fight, report_id, report, char_name, abilities, out_dir)
        if not ret:
            print(f'Skipped processing {fight.id} from {report_id}')

def get_report_ids(args):
    ids = []
    with open(args.file, 'r') as file:
        lines = file.readlines()

    for line in lines:
        ids.append(line.strip())

    return ids

def main(argv):
    config_file = '.config.yaml'
    if not os.path.isfile(config_file):
        print('Error: .config.yaml doesn\'t exist')
        return -1

    with open(config_file, 'r') as file:
        client_config = yaml.safe_load(file)

    if 'client_id' not in client_config or 'client_secret' not in client_config:
        print(f'{config_file} must have client_id field and client_secret field. See README.md for details.')
        return -1

    parser = argparse.ArgumentParser()
    parser.add_argument('--name', '-n', type=str, required=True,
                        help='Name of character to filter for')
    parser.add_argument('--cont', '-c', type=int, metavar='continue',
                        help='0-based index of report to resume from')
    parser.add_argument('--file', '-f', type=str, required=True,
                        help='File with list of report IDs to process')
    parser.add_argument('--output', '-o', type=str, required=True,
                        help='Directory to output processed data to')
    args = parser.parse_args(argv[1:])

    client = FFLogsClient(client_config['client_id'], client_config['client_secret'])

    report_ids = get_report_ids(args)
    resume = args.cont if args.cont else 0

    if not os.path.isdir(args.output):
        print(f'Output directory "{args.output}" doesn\'t exist, creating it')
        os.mkdir(args.output)

    for i, report_id in enumerate(report_ids):
        if i < resume:
            continue
        process_report(client, report_id, args.name, args.output)
        print(f'Done processing report {report_id} (index {i})')

if __name__ == '__main__':
    sys.exit(main(sys.argv))
