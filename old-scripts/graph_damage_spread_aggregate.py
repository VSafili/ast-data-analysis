#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# graph_damage_spread_aggregate.py - Generate histograms for varying numbers of malefic

import lib

import argparse
from matplotlib import pyplot as plt
from pathlib import Path
import sys
import yaml

# These are my stats for DSR
job = 'Astrologian'
skill = 'Fall Malefic'
mind = 2716
det = 1.099
master_stats_key = f'{mind}@0.227x1.577,0.01x1.25,{det}'
width = 14

def draw_graphs(output_dict, output_dir, skip_str):
    for i in output_dict:
        data = output_dict[i]

        title = f'Damage spread: {skill} x{i} ({master_stats_key})'
        s = skill.lower().replace(' ', '_')
        lib.draw_damage_histogram(data, Path(output_dir) / f'{s}-{i}{skip_str}.png', title, 50)

def initialize_dict(d):
    for i in range(1, width + 1):
        d[i] = []

def process_data(data, output_dict, skip_rate_buffs):
    main_stat = data['stats']['main_stat']
    crit_rate = data['stats']['crit_rate']
    crit_mult = data['stats']['crit_mult']
    dh_rate = data['stats']['dh_rate']
    det_mult = data['stats']['det_mult']
    stats_key = f'{main_stat}@{crit_rate}x{crit_mult},{dh_rate}x1.25,{det_mult}'

    if data['stats']['job'] != job or stats_key != master_stats_key:
        return

    collector = {}
    initialize_dict(collector)

    for event in data['events']:
        if event['name'] != skill:
            continue

        if skip_rate_buffs and \
           (event['crit_rate'] != crit_rate or event['dh_rate'] != dh_rate):
            continue

        amount = int(event['amount'] / event['multiplier'])

        for i in collector:
            collector[i].append(amount)
            if len(collector[i]) > i:
                collector[i].pop(0)
            if len(collector[i]) == i:
                output_dict[i].append(sum(collector[i]))

def main(argv):
    parser = lib.common_parser()
    parser.add_argument('--skip-rate-buffs', '-s', action='store_true',
                        required=False, dest='skip_rate_buffs',
                        help='Skip events that have crit or dh rate buffs')
    args = parser.parse_args(argv[1:])

    lib.create_path_if_not_exist(args.output)

    output_dict = {}
    initialize_dict(output_dict)

    for data in lib.iterate_collection(args.input):
        process_data(data, output_dict, args.skip_rate_buffs)

    skip = '-skip' if args.skip_rate_buffs else ''

    with open(Path(args.output) / f'samples{skip}.yaml', 'w') as outfile:
        yaml.dump(output_dict, outfile)

    draw_graphs(output_dict, args.output, skip)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
