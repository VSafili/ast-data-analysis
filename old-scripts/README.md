# Astrological Data Analysis for FF14

I made this project for two purposes:
- To analyze the independency of crit/dh rates
- To do statistical analysis of damage variance on experimental data 

As this project was made for me, there are a bunch of features that I have not
implemented as I didn't need them. I will point them out where necessary
(assuming I remember).

## Requirements

Required packages:

- fflogsapi
- matplotlib
- numpy
- pyyaml

`requirements.txt` can be used to install them (including dependencies) all in
one go with `pip3 install -r requirements.txt`.

If you want to run `collect.py` yourself (see below), then a `.config.yaml`
file is required to use the FFLogs API, which needs to be filled in with the
client ID and client secret. Here's an example of the contents of `.config.yaml`:

```
client_id: 12345678-9abc-def0-1234-56789abcdef0
client_secret: dHdpbGlnaHQgcG9wb3RvIHNhbGFkdHdpbGlnaHQg
```

I have added `.config.yaml` to `.gitignore` so that I don't have to worry about
accidentally commiting the client secret.

All scripts in this project are assumed to be run from the project directory.

## Collection scripts

### collect.py

This is a script that will gather character stats and damage events from a list
of FFLogs report IDs. Example usage:

`./collect.py -f list -n "Tataru Taru" -o out`

This collects data from the list of report IDs specified in the file `list`,
and filters for the character named "Tataru Taru", and outputs the collected
data into a directory called `out`. The directory is created if it doesn't
exist.

As I expected the gathering to be interrupted frequently (either due to errors
or API ratelimiting), the script will output the last completed report ID index:

`Done processing report aHdh5vPrCnfqMtbk (index 59)`

So that you can feed the next one into a `cont` parameter like so:

`./collect.py -f list -n "Tataru Taru" -o out -c 60`

The content of the `list` file is simply a list of FFLogs report IDs, for example:

```
GW4mT8KbvaKh78Rz
aHdh5vPrCnfqMtbk
zGaWYjCNzbTwpah5
```

I've added filters such that the script will only consider data when the
specified character is a black mage, summoner, astrologian, or scholar. It will
also only consider encounters for TOP and DSR. Changing these should be trivial
but it's not something I need so it isn't implemented for the time being.

The output directory can optionally be manually compressed into a tar file (of
any compression type) and the analysis scripts will be able to handle it. The
output directory can also be used directly.

### collect-raw.py

This script is basically the same as `collect.py`, except that it will not
process the data. It gathers the abilities, character stats, and all damage
events, and saves them as python pickles.

## Analysis scripts

Unless otherwise specified, all analysis scripts work on the processed output
from collect.py.

The -i and -o parameters are common to all analysis scripts. -i is able to take
the output directory from collect.py, or a tarfile version of it. -o is output
directory or file, depending on the script, and the contents will always be
overwritten if it already exists. If it is meant to be directory the directory
(and any child directories) will automatically be created if it doesn't already
exist.

### count.py

Output type: file

This is a script that will count the number of occurences of a given skill
within the data collected from collect.py. It also serves as a sample on how to
write analysis scripts.

```
$ ./count.py -n "Fall Malefic" -i data.tar.gz
Total count of Fall Malefic: 176457
```

### extract_damage_samples.py

Output type: file

This script extracts all the damage samples from the output of the collect.py
and saves it to a yaml file. It groups the samples by job, skill, stats, and
crit/dh. It is a two-level dictionary, for example:

```
Astrologian-Fall Malefic:
  2688@0.217x1.567,0.01x1.25,1.115:
    cdh:
    - 15247
    - 14641
    crit:
    - 11947
    - 11903
    dh:
    - 9569
    - 9008
    none:
    - 7775
    - 7133
Black Mage-Blizzard:
  2700@0.083x1.433,0.305x1.25,1.076:
    cdh:
    - 7543
    crit: []
    dh: []
    none:
    - 4586
    - 4279
  2700@0.083x1.433,0.306x1.25,1.076:
    cdh: []
    crit:
    - 9056
    dh: []
    none:
    - 6468
    - 6339
    - 6056
Black Mage-Blizzard III:
  2700@0.083x1.433,0.289x1.25,1.076:
    cdh: []
    crit: []
    dh: []
    none:
    - 6475
  2700@0.083x1.433,0.305x1.25,1.076:
    cdh:
    - 11478
    - 10527
```

The key of the second level dictionary is
`{main stat}@{crit rate}x{crit multiplier},{dh rate}x1.25,{det multiplier}`.

The output can be used to graph histograms, for example.

### investigate_malefic_top.py

Output type: directory

This is a script that I am using to investigate the high shoulder on the damage
spread for malefic on TOP. It currently outputs:
- `data.yaml`, where all the damage samples are grouped by day and by "full" or
  "trunc" (for all damage events and events with bug omega filtered out,
  respectively)
- damage histograms for full and trunc grouped by day

## License

Unless otherwise specified, this project is licensed under the the GNU General
Public License version 2.0 or later.
