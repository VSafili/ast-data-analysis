#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# collect.py - Script to gather character stats and damage events from a list of FFLogs reports

import argparse
from datetime import datetime
from fflogsapi import FFLogsClient, GQLEnum
import math
import os
from pathlib import Path
import sys
import yaml

id_dsr = 45
id_top = 53

rate_buffs = {
    'crit': {
        1001825: 0.2, # devilment
        1001221: 0.1, # chain
        1000786: 0.1, # litany
        1002216: 0.02 # minuet
    },

    'dh': {
        1001825: 0.2, # devilment
        1000141: 0.2, # battle voice
        1002218: 0.03, # paeon
    }
}

def crit_rate(crit):
    return (int(200 * (crit - 400) / 1900) + 50) / 1000

def crit_mult(crit):
    return (int(200 * (crit - 400) / 1900) + 1400) / 1000

def dh_rate(dh):
    return int(550 * (dh - 400) / 1900) / 1000

def det_mult(det):
    return (1000 + int(140 * (det - 390) / 1900)) / 1000

def sps_mult(sps):
    return (1000 + int(130 * (sps - 400) / 1900)) / 1000

def sps_gcd(sps):
    return int(2500 * (1000 + math.ceil(130 * (400 - 2171) / 1900)) / 10000) / 100

# Get crit/dh rate, taking into account crit/dh rate buffs if applicable
def get_h_rate(t, stats, event):
    rate = stats[f'{t}_rate']
    if 'buffs' not in event:
        return rate

    buff_ids = [int(b) for b in event['buffs'].split('.') if b != '']
    for buff in rate_buffs[t]:
        if buff in buff_ids:
            rate += rate_buffs[t][buff]
    return rate

def get_char_stats(fight, report, char_name):
    # There could be multiple instances of the character if for example they
    # participated in different encounters with different jobs in the same report
    chars = [actor for actor in report.actors() if actor.name == char_name]
    if len(chars) < 1:
        print('No character')
        return None

    for char_info in chars:
        stats = fight.events({'sourceID': char_info.id, 'dataType': GQLEnum('CombatantInfo')})
        if stats is None:
            print('No combatant info (GQL)')
            return None
        if len(stats) > 0:
            break
    if len(stats) < 1:
        print('No combatant info (none found)')
        return None
    char_stats = stats[0]

    # Character didn't participate in combat
    if char_info.job is None:
        print('Character did not participate in combat')
        return None

    # Just in case, block all other jobs
    if char_info.job.name != 'Black Mage' and char_info.job.name != 'Summoner' and \
       char_info.job.name != 'Astrologian' and char_info.job.name != 'Scholar':
        print('Unsupported job "{char_info.job.name}"')
        return None

    if 'attackMagicPotency' not in char_stats:
        print('Warning: Stats unavailable')
        return {
            'id': char_info.id,
            'name': char_info.name,
            'job':  char_info.job.name,
            'main_stat': -1,
            'crit_rate': -1,
            'crit_mult': -1,
            'dh_rate':   -1,
            'det_mult':  -1,
            'sps_mult':  -1,
            'gcd':       -1,
        }

    return {
        'id': char_info.id,
        'name': char_info.name,
        'job':  char_info.job.name,
        'main_stat': char_stats['attackMagicPotency'],
        'crit_rate': crit_rate(char_stats['criticalHit']),
        'crit_mult': crit_mult(char_stats['criticalHit']),
        'dh_rate':   dh_rate(char_stats['directHit']),
        'det_mult':  det_mult(char_stats['determination']),
        'sps_mult':  sps_mult(char_stats['spellSpeed']),
        'gcd':       sps_gcd(char_stats['spellSpeed']),
    }

def write_to_file(out_dir, report_id, report, fight, stats, events):
    name = stats['name'].split(' ')[0].lower()
    job = stats['job'].lower().replace(' ', '_')
    date = datetime.fromtimestamp(report.start_time() / 1000).strftime('%Y%m%d')

    out_path = Path(out_dir) / f'{date}_{report_id}_{fight.id:02d}_{name}_{job}.yaml'
    with open(out_path, 'w') as outfile:
        yaml.dump({'stats': stats, 'events': events}, outfile)

def process_fight(fight, report_id, report, char_name, abilities, out_dir):
    try:
        zone_id = fight.encounter().zone().id
    except:
        print('Failed to get zone id')
        return False
    if zone_id != id_dsr and zone_id != id_top:
        print('Zone is not TOP or DSR')
        return False

    stats = get_char_stats(fight, report, char_name)
    if stats is None:
        return False

    events = []
    damage_events = fight.events({'sourceID': stats['id'], 'dataType': GQLEnum('DamageDone')})
    for event in [e for e in damage_events if e['type'] == 'calculateddamage']:
        events.append({
            'name': abilities[event['abilityGameID']].name,
            'amount': event['amount'],
            'multiplier': event['multiplier'],
            'crit': True if event['hitType'] == 2 else False,
            'dh': True if 'directHit' in event and event['directHit'] else False,
            'crit_rate': get_h_rate('crit', stats, event),
            'dh_rate': get_h_rate('dh', stats, event),
        })

    write_to_file(out_dir, report_id, report, fight, stats, events)
    return True

def process_report(client, report_id, char_name, out_dir):
    report = client.get_report(report_id)
    fights = [report.fight(i) for i in range(1, report.fight_count() + 1)]
    abilities = {e.game_id: e for e in report.abilities()}
    for fight in fights:
        print(f'Processing fight {fight.id} from report {report_id}...')
        ret = process_fight(fight, report_id, report, char_name, abilities, out_dir)
        if not ret:
            print(f'Skipped processing {fight.id} from {report_id}')

def get_report_ids(args):
    ids = []
    with open(args.file, 'r') as file:
        lines = file.readlines()

    for line in lines:
        ids.append(line.strip())

    return ids

def main(argv):
    config_file = '.config.yaml'
    if not os.path.isfile(config_file):
        print('Error: .config.yaml doesn\'t exist')
        return -1

    with open(config_file, 'r') as file:
        client_config = yaml.safe_load(file)

    if 'client_id' not in client_config or 'client_secret' not in client_config:
        print(f'{config_file} must have client_id field and client_secret field. See README.md for details.')
        return -1

    parser = argparse.ArgumentParser()
    parser.add_argument('--name', '-n', type=str, required=True,
                        help='Name of character to filter for')
    parser.add_argument('--cont', '-c', type=int, metavar='continue',
                        help='0-based index of report to resume from')
    parser.add_argument('--file', '-f', type=str, required=True,
                        help='File with list of report IDs to process')
    parser.add_argument('--output', '-o', type=str, required=True,
                        help='Directory to output processed data to')
    args = parser.parse_args(argv[1:])

    client = FFLogsClient(client_config['client_id'], client_config['client_secret'])

    report_ids = get_report_ids(args)
    resume = args.cont if args.cont else 0

    if not os.path.isdir(args.output):
        print(f'Output directory "{args.output}" doesn\'t exist, creating it')
        os.mkdir(args.output)

    for i, report_id in enumerate(report_ids):
        if i < resume:
            continue
        process_report(client, report_id, args.name, args.output)
        print(f'Done processing report {report_id} (index {i})')

if __name__ == '__main__':
    sys.exit(main(sys.argv))
