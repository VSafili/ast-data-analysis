#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# extract_damage_samples.py - Script to extract damage samples

import lib

import sys
import yaml

def create_if_not_exist(output_dict, key1, key2):
    if key1 not in output_dict:
        output_dict[key1] = {}

    if key2 not in output_dict[key1]:
        output_dict[key1][key2] = {}

    for value in ['none', 'crit', 'dh', 'cdh']:
        if value not in output_dict[key1][key2]:
            output_dict[key1][key2][value] = []

def process_data(data, output_dict):
    job = data['stats']['job']
    main_stat = data['stats']['main_stat']
    crit_rate = data['stats']['crit_rate']
    crit_mult = data['stats']['crit_mult']
    dh_rate = data['stats']['dh_rate']
    det_mult = data['stats']['det_mult']
    stats_key = f'{main_stat}@{crit_rate}x{crit_mult},{dh_rate}x1.25,{det_mult}'

    for event in data['events']:
        name = event['name']
        skill_key = f'{job}-{name}'
        amount = int(event['amount'] / event['multiplier'])
        create_if_not_exist(output_dict, skill_key, stats_key)

        if event['crit'] and event['dh']:
            output_dict[skill_key][stats_key]['cdh'].append(amount)
        elif event['crit']:
            output_dict[skill_key][stats_key]['crit'].append(amount)
        elif event['dh']:
            output_dict[skill_key][stats_key]['dh'].append(amount)
        else:
            output_dict[skill_key][stats_key]['none'].append(amount)

def main(argv):
    args = lib.common_parser().parse_args(argv[1:])

    output_dict = {}
    for data in lib.iterate_collection(args.input):
        process_data(data, output_dict)

    with open(args.output, 'w') as outfile:
        yaml.dump(output_dict, outfile)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
