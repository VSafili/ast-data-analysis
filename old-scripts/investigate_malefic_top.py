#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# investigate_malefic_top.py - Investigate the discrepancy in malefic damage spread in TOP

import lib

import argparse
from matplotlib import pyplot as plt
import numpy as np
import os
from pathlib import Path
import sys
import yaml

job = 'Astrologian'
skill = 'Fall Malefic'
mind = 3319
det = 1.093
master_stats_key = f'{mind}@0.247x1.597,0.01x1.25,{det}'
thres = 9200
limit_lo = 8200
limit_hi = 9500

def filter_data(data, limits):
    return data[(limits[0] < data) & (data < limits[1])]

def draw_graphs(output_dict, output_dir):
    for date in output_dict.keys():
        full_data = np.array(output_dict[date]['full'])
        full_data = filter_data(full_data, [limit_lo, limit_hi])

        trunc_data = np.array(output_dict[date]['trunc'])
        trunc_data = filter_data(trunc_data, [limit_lo, limit_hi])

        title = f'{date} {skill} (no crit nor dh) ({mind}x{det})'
        lib.draw_damage_histogram(full_data,
                                  Path(output_dir) / 'full' / f'{date}-graph.png',
                                  f'{title} full', 50)
        lib.draw_damage_histogram(trunc_data,
                                  Path(output_dir) / 'trunc' / f'{date}-graph.png',
                                  f'{title} trunc', 50)

def process_data(filename, data, output_dict):
    date = filename.split('_')[0]

    main_stat = data['stats']['main_stat']
    crit_rate = data['stats']['crit_rate']
    crit_mult = data['stats']['crit_mult']
    dh_rate = data['stats']['dh_rate']
    det_mult = data['stats']['det_mult']
    stats_key = f'{main_stat}@{crit_rate}x{crit_mult},{dh_rate}x1.25,{det_mult}'

    if data['stats']['job'] != job or stats_key != master_stats_key:
        return

    filter_count = 0
    for event in data['events']:
        if event['name'] != skill or event['crit'] or event['dh']:
            continue

        amount = int(event['amount'] / event['multiplier'])

        if date not in output_dict:
            output_dict[date] = {'full': [], 'trunc': []}
        output_dict[date]['full'].append(amount)

        if filter_count > 47:
            output_dict[date]['trunc'].append(amount)
        filter_count += 1

def main(argv):
    parser = lib.common_parser()
    args = parser.parse_args(argv[1:])

    for path in [args.output, Path(args.output) / 'full', Path(args.output) / 'trunc']:
        if not os.path.isdir(path):
            os.mkdir(path)

    output_dict = {}
    for fn, data in lib.iterate_collection_with_filename(args.input):
        process_data(fn, data, output_dict)

    with open(Path(args.output) / 'data.yaml', 'w') as outfile:
        yaml.dump(output_dict, outfile)

    draw_graphs(output_dict, args.output)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
