# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# lib.py - Common library for processing scripts

import argparse
from matplotlib import pyplot as plt
import os
from pathlib import Path
import sys
import tarfile
import yaml

def iterate_collection_with_filename(file):
    if os.path.isfile(file):
        with tarfile.open(file, 'r') as tar:
            members = tar.getmembers()
            for tarinfo in [member for member in members if member.isfile()]:
                yield [os.path.basename(tarinfo.name), yaml.safe_load(tar.extractfile(tarinfo).read())]

    if os.path.isdir(file):
        for log in os.listdir(file):
            yield [log, yaml.safe_load((Path(file) / log).read_text())]

def iterate_collection(file):
    if os.path.isfile(file):
        with tarfile.open(file, 'r') as tar:
            members = tar.getmembers()
            for tarinfo in [member for member in members if member.isfile()]:
                yield yaml.safe_load(tar.extractfile(tarinfo).read())

    if os.path.isdir(file):
        for log in os.listdir(file):
            yield yaml.safe_load((Path(file) / log).read_text())

def common_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i', type=str, required=True,
                        help='Directory or (compressed) tarfile of data collected from collect.py')
    parser.add_argument('--output', '-o', type=str, required=True,
                        help='File or directory to output the result to')
    return parser

def set_stdout(output):
    if output:
        sys.stdout = open(output, 'w')

def create_path_if_not_exist(path):
    if not os.path.isdir(path):
        print(f'Output directory "{path}" doesn\'t exist, creating it')
        os.mkdir(path)

def draw_damage_histogram(data, path, title, num_bins):
    plt.clf()
    plt.hist(data, bins=num_bins)
    plt.xlabel('Damage')
    plt.ylabel('Samples')
    plt.title(title)
    plt.grid()
    plt.savefig(path)
