#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# count.py - Script to count the number of instances of a skill

import lib

import argparse
import sys

def process_data(data, name):
    counter = 0
    for event in data['events']:
        if event['name'] == name:
            counter += 1
    return counter

def main(argv):
    parser = lib.common_parser()
    parser.add_argument('--name', '-n', type=str, required=True,
                        help='Name of skill to filter for')
    args = parser.parse_args(argv[1:])

    counter = 0
    for data in lib.iterate_collection(args.input):
        counter += process_data(data, args.name)

    lib.set_stdout(args.output)
    print(f'Total count of {args.name}: {counter}')

if __name__ == '__main__':
    sys.exit(main(sys.argv))
