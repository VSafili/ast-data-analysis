#!/usr/bin/env python3

import argparse
import sqlite3
from suffix_tree import Tree
import sys

# Unlike collect-sqlite.py, this script cannot be run multiple times, due to
# needing to keep validity between sequence infos / sequence instances and
# their members

# Members are tuples where the first index is the key
def merge_lists(list1, list2):
    if len(list1) == 0:
        return list2
    if len(list2) == 0:
        return list1

    ret = []
    while len(list1) > 0 and len(list2) > 0:
        if len(list1) == 0:
            ret.append(list2.pop(0))
            continue
        if len(list2) == 0:
            ret.append(list1.pop(0))
            continue

        if list1[0][0] <= list2[0][0]:
            ret.append(list1.pop(0))
        elif list1[0][0] > list2[0][0]:
            ret.append(list2.pop(0))

    return ret

def get_reports(conn, zone, job):
    cur = conn.cursor()
    query = '''select distinct report_id
               from events inner join abilities on events.ability_id = abilities.id
               where events.zone_id = ? and abilities.job = ?;'''
    cur.execute(query, (zone, job))
    return [tup[0] for tup in cur.fetchall()]

def get_fights(conn, report_id):
    cur = conn.cursor()
    query = 'select distinct fight_id from fights where report_id = ?;'
    cur.execute(query, (report_id,))
    return [tup[0] for tup in cur.fetchall()]

def get_events(conn, report_id, fight_id, job, zone_id):
    queries = [
        # Non-dot damage events (excluding multi-hit events, including Dream Within a Dream and aoe)
        '''select timestamp_local, events.id, ability_id, standardized_potency, damage, multiplier, crit_rate_buff, dh_rate_buff
           from events inner join abilities on events.ability_id = abilities.id
           where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is null and ability_id != 3566
           group by packet_id having count(*) = 1;''',

        # Multi-hit non-dot damage events (aoe) (exclude Dream Within a Dream) - select the maximum
        '''select min(timestamp_local), events.id, ability_id, standardized_potency, max(damage), multiplier, crit_rate_buff, dh_rate_buff
           from events inner join abilities on events.ability_id = abilities.id
           where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is null and ability_id != 3566
           group by packet_id having count(*) > 1;''',

        # Aggregated dot damage events
        '''select min(timestamp_local), events.id, ability_id, standardized_potency, sum(damage), multiplier, crit_rate_buff, dh_rate_buff
           from events inner join abilities on events.ability_id = abilities.id
           where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is not null
           group by dot_info;'''
    ]

    if job == 'Ninja':
        queries += [
            # Multi-hit non-dot damage events (Dream Within a Dream)
            '''select min(timestamp_local), events.id, ability_id, standardized_potency, sum(damage), multiplier, crit_rate_buff, dh_rate_buff
               from events inner join abilities on events.ability_id = abilities.id
               where report_id = ? and fight_id = ? and job = ? and zone_id = ? and dot_info is null and ability_id = 3566
               group by packet_id having count(*) > 1;''',
        ]


    results = []
    cur = conn.cursor()
    for query in queries:
        cur.execute(query, (report_id, fight_id, job, zone_id))
        results.append(cur.fetchall())

    merged = []
    for result in results:
        merged = merge_lists(merged, result)

    return [ [(entry[2], entry[6], entry[7]) for entry in merged], merged ]

def insert_sequence_infos(conn, zone, job, path, num_matches, counter):
    name = f'{job} zone {zone} seq len {len(path)} count {num_matches} number {counter}'

    counts = {}
    std_potency = 0
    # We can't iterate path directly as it'll go past the match
    for i in range(len(path)):
        # Count occurences
        key = path[i]
        if key not in counts:
            counts[key] = 0
        counts[key] += 1

        # Accumulate potencies
        ability_id = path[i][0]
        cur = conn.cursor()
        cur.execute('select standardized_potency from abilities where id = ?', (ability_id,))
        res = cur.fetchone()
        std_potency += res[0]

    conn.isolation_level = None
    cur = conn.cursor()
    # Insert sequence info
    try:
        cur.execute('begin')
        cur.execute('insert into sequence_infos (name, standardized_potency) values (?, ?)', (name, std_potency))
        seq_id = cur.lastrowid
        for key in counts.keys():
            cur.execute('''insert into sequence_info_abilities (seq_id, ability_id, crit_rate_buff, dh_rate_buff, count)
                           values (?, ?, ?, ?, ?)''', (seq_id, key[0], key[1], key[2], counts[key]))
        cur.execute('commit')
    except:
        seq_id = None
        cur.execute('rollback')
    conn.isolation_level = 'DEFERRED'
    return seq_id

def insert_sequence_instance(conn, seq_id, matched_events):
    damage_std = sum([e[4] / e[5] for e in matched_events])
    conn.isolation_level = None
    cur = conn.cursor()
    try:
        cur.execute('begin')
        cur.execute('insert into sequence_instances (first_event_id, seq_id, damage_std) values (?, ?, ?)',
                    (matched_events[0][1], seq_id, damage_std))
        seq_instance_id = cur.lastrowid
        for event in matched_events:
            cur.execute('insert into sequence_instance_events (seq_instance_id, seq_info_id, event_id) values (?, ?, ?)',
                        (seq_instance_id, seq_id, event[1]))
        cur.execute('commit')
    except:
        cur.execute('rollback')
    conn.isolation_level = 'DEFERRED'

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--database', '-d', type=str, required=True,
                        help='Database file from which to aggregate sequences and to which to save sequences')
    args = parser.parse_args(argv[1:])

    conn = sqlite3.connect(args.database)

    zones = [ 53 ]
    jobs = [ 'Astrologian',
             'Scholar',
             'Dark Knight',
             'Paladin',
             'Reaper',
             'Ninja',
             'Black Mage',
             'Red Mage',
             'Dancer',
             'Gunbreaker',
             'White Mage',
             'Sage',
             'Bard',
             'Machinist',
             'Summoner',
             'Samurai',
             'Monk'
    ]

    for zone in zones:
        for job in jobs:
            tree = Tree()
            all_events = {}
            reports = get_reports(conn, zone, job)

            for report_id in reports:
                fights = get_fights(conn, report_id)
                for fight_id in fights:
                    suffix_events, events = get_events(conn, report_id, fight_id, job, zone)
                    key = (report_id, fight_id, zone, job)
                    # This lets us map suffix tree entries to list of events
                    all_events[key] = events
                    tree.add(key, suffix_events)

            counter = 0
            for k, length, path in tree.common_substrings():
                if length < 6:
                    continue
                matches = tree.find_all(path)
                # We need at least 8 samples for Anderson-Darling test
                if len(matches) < 8:
                    continue
                # 200 samples is probably the maximum number of samples that we
                # can use for the Anderson-Darling test
                if len(matches) > 200:
                    continue
                print(f'MATCHED sequence (count {len(matches)}) (len {len(path)})')

                counter += 1
                seq_id = insert_sequence_infos(conn, zone, job, path, len(matches), counter)
                if seq_id is None:
                    print(f'ERROR Insertion failed for path {path}')
                    continue

                for m in matches:
                    key = m[0]
                    matched_path = m[1]
                    matched_events = all_events[key][matched_path.start:(matched_path.start+len(path))]
                    insert_sequence_instance(conn, seq_id, matched_events)

    conn.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
