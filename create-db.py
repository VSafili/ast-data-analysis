#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024, Vaal Safili <vaal.safili@gmail.com>
#
# create-db.py - Create the database file

import argparse
import sys
import sqlite3

# select events.fight_id, events.timestamp_local, events.damage, events.multiplier, events.crit, events.crit_rate_buff, abilities.name from events inner join abilities on events.ability_id = abilities.id where events.timestamp_local < 60;

# select events.timestamp_local, events.damage, events.multiplier, events.crit, abilities.name from events inner join buffs on events.id = buffs.event_id inner join abilities on buffs.buff_id = abilities.id where events.fight_id = 21;

create_tables = [
        # id matches the game
        '''create table if not exists enemies (
                id integer primary key on conflict ignore,
                name text not null,
                comment text
        ); ''',

        # id matches the game
        '''create table if not exists zones (
                id integer primary key on conflict ignore,
                name text not null,
                comment text
        ); ''',

        # timestamp is ISO8601 "YYYY-MM-DD HH:MM:SS.SSS"
        '''create table if not exists fights (
                report_id text not null,
                fight_id integer not null,
                zone_id integer not null,
                timestamp text not null,
                comment text,
                primary key (report_id, fight_id) on conflict ignore,
                foreign key (zone_id) references zones(id)
        ); ''',

        # id matches the game
        # type is internal to the game, but we save it just in case
        '''create table if not exists abilities (
                id integer primary key on conflict ignore,
                name text not null,
                potency integer,
                standardized_potency integer,
                type integer,
                job text,
                guaranteed_crit integer default 0,
                guaranteed_dh integer default 0,
                is_buff integer not null default 0,
                crit_buff_amount integer default 0,
                dh_buff_amount integer default 0
        ); ''',

        # id does not match the game
        '''create table if not exists actors (
                id integer primary key autoincrement,
                name text not null,
                job_id text not null,
                zone_id integer not null,
                main_stat integer,
                crit_stat integer,
                det_stat integer,
                dh_stat integer,
                comment text not null,
                foreign key (zone_id) references zones (id),
                unique (name, job_id, zone_id, comment) on conflict ignore
        ); ''',

        '''create table if not exists dot_infos (
                id integer primary key autoincrement,
                finalized_amount real,
                expected_amount integer,
                expected_crit_rate real,
                actor_potency_ratio real,
                guess_amount real,
                direct_hit_percentage real,
                report_id text not null,
                fight_id integer not null,
                packet_id integer,
                foreign key (report_id, fight_id) references fights (report_id, fight_id),
                unique (report_id, fight_id, packet_id) on conflict ignore
        ); ''',

        # timestamp_global is ISO8601 "YYYY-MM-DD HH:MM:SS.SSS"
        # timestamp_local is number of seconds (with ms precision) since pull
        # crit, dh, and dot_tick are booleans
        '''create table if not exists events (
                id integer primary key autoincrement,
                timestamp_global text not null,
                timestamp_local real not null,
                ability_id integer not null,
                damage integer not null,
                multiplier real not null,
                crit_rate_buff integer not null,
                dh_rate_buff integer not null,
                crit integer not null,
                dh integer not null,
                zone_id integer not null,
                actor_id integer not null,
                target_id integer not null,
                report_id text not null,
                fight_id integer not null,
                dot_tick integer not null,
                dot_info integer,
                dot_origin integer,
                packet_id integer,
                foreign key (ability_id) references abilities (id),
                foreign key (zone_id) references zones (id),
                foreign key (actor_id) references actors (id),
                foreign key (target_id) references enemies (id),
                foreign key (report_id, fight_id) references fights (report_id, fight_id),
                foreign key (dot_info) references dot_infos (id),
                foreign key (dot_origin) references events(id),
                unique (report_id, fight_id, timestamp_global, actor_id, ability_id) on conflict ignore
        ); ''',

        '''create table if not exists buffs (
                id integer primary key autoincrement,
                event_id integer not null,
                buff_id integer not null,
                foreign key (event_id) references events (id),
                foreign key (buff_id) references abilities (id),
                unique (event_id, buff_id) on conflict ignore
        ); ''',

        # timestamp_global is ISO8601 "YYYY-MM-DD HH:MM:SS.SSS"
        # timestamp_local is number of milliseconds since pull
        '''create table if not exists cards (
                id integer primary key autoincrement,
                report_id text not null,
                fight_id integer not null,
                zone_id integer not null,
                timestamp_global text not null,
                timestamp_local real not null,
                target_job text not null,
                card_name text not null,
                damage_raw integer not null,
                damage_buffed integer not null,
                card_correct integer not null,
                melee_card integer not null,
                astrologian_id integer not null,
                foreign key (report_id, fight_id) references fights (report_id, fight_id),
                foreign key (zone_id) references zones (id),
                foreign key(astrologian_id) references actors (id),
                unique (report_id, fight_id, timestamp_global) on conflict ignore
        ); ''',

        '''create table if not exists card_buffs (
                id integer primary key autoincrement,
                timestamp_global text not null,
                timestamp_local real not null,
                card_id integer not null,
                ability_id integer not null,
                damage integer not null,
                multiplier real not null,
                crit_rate_buff integer not null,
                dh_rate_buff integer not null,
                crit integer not null,
                dh integer not null,
                foreign key (card_id) references cards (id),
                foreign key (ability_id) references abilities (id),
                unique (timestamp_global, card_id, ability_id) on conflict ignore
        ); ''',

        '''create table if not exists sequence_infos (
            id integer primary key autoincrement,
            name text not null,
            unique (name) on conflict rollback
        );''',

        '''create table if not exists sequence_info_abilities (
            id integer primary key autoincrement,
            seq_id integer not null,
            ability_id integer not null,
            crit_rate_buff integer not null,
            dh_rate_buff integer not null,
            count integer not null default 1,
            foreign key (seq_id) references sequence_infos (id),
            foreign key (ability_id) references abilities (id),
            unique (seq_id, ability_id, crit_rate_buff, dh_rate_buff) on conflict rollback
        );''',

        '''create table if not exists sequence_instances (
            id integer primary key autoincrement,
            first_event_id integer not null,
            seq_id integer not null,
            damage_std integer not null,
            standardized_potency integer not null,
            foreign key (first_event_id) references events (id),
            foreign key (seq_id) references sequence_infos (id),
            unique (first_event_id, seq_id) on conflict rollback
        );''',

        '''create table if not exists sequence_instance_events (
            id integer primary key autoincrement,
            seq_instance_id integer not null,
            seq_info_id integer not null,
            event_id integer not null,
            foreign key (seq_instance_id) references sequence_instances (id),
            foreign key (seq_info_id) references sequence_infos (id),
            foreign key (event_id) references events (id),
            unique (seq_instance_id, event_id) on conflict rollback,
            unique (seq_info_id, event_id) on conflict rollback
        );''',
]

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', '-o', type=str, required=True,
                        help='Name of file to write the sqlite database to')
    args = parser.parse_args(argv[1:])

    conn = sqlite3.connect(args.output)
    cur = conn.cursor()
    for create_table in create_tables:
        cur.execute(create_table)

    zones = {
        45: 'Dragonsong\'s Reprise',
        53: 'The Omega Protocol',
        62: 'AAC LH',
    }
    add_zone_query = 'insert into zones (id, name) values (?, ?);'
    for zone_id in zones:
        cur.execute(add_zone_query, (zone_id, zones[zone_id]))
    conn.commit()

    conn.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
