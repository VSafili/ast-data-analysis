# アルカナの極意の処理コード

当プロジェクトは「アルカナの極意」の同人誌シリーズのデータ処理用に
使ったコードのレポジトリである。

主な使い道は某戦闘記録サイトからデータを取得して、
統計解析しやすいようにデータベース（sqlite3）に保存すること。
他に統計解析用のスクリプト等もあったりする。

## インストール

データ取得部分は python で書かれてる。
必要なパッケージは `requirements.txt` に入ってるからそこから
自身のパイソン環境でインストールするとよい。

素で使ってるなら `pip3 install -r requirements.txt` で
依存パッケージを全部一気にインストールできる。

## データ取得

某戦闘記録サイトからデータを取得するには `collect-sqlite.py` の
スクリプトを使う。

### APIキー

APIキーは `.config.yaml` というファイルに次のように保存する：

```
client_id: 12345678-9abc-def0-1234-56789abcdef0
client_secret: dHdpbGlnaHQgcG9wb3RvIHNhbGFkdHdpbGlnaHQg
```

`.config.yaml` は `.gitignore` に入れてあるから間違えてコミット
してしまう心配はない。

### データベース初期化

取得スクリプトはデータをデータベースに保存するからまずデータベース
ファイルを初期化する必要がある：

`$ ./create-db.py -o data.db`

これで `data.db` というファイルに sqlite3 のデータベースを作って
初期化する。すでに存在していた場合は上書きされるから注意。

### 入力設定

次はどの戦闘記録を取得するのかを設定する必要がある。この設定は
次のように yaml ファイルに書く：

```
zone: 53
characters:
- character:
    name: Taro Gakusha
    job: Scholar
    match: both
    comment: Example comment
  stats:
    main: 3104
    crit: 2429
    det: 1523
    dh: 508
- character:
    name: Jiro Sensei
    job: Astrologian
    match: both
reports:
- GW4mT8KbvaKh78Rz
- aHdh5vPrCnfqMtbk
- zGaWYjCNzbTwpah5
```

`zone` は某戦闘記録サイトの url の `zone` のパラメータからとったもの。
代表的なのは暁月時代のオメガは53、暁月時代の竜詩は45、
アルカディアライトヘビー級（難易度問わず）は62。
黄金時代の過去絶は全部同じゾーンになっていて、全部59。

`character` のブロックには `name` と `job` と `match` は必須。

- `job` はスペルとスペースと大文字小文字がマッチしないといけない。
  - (`collect-sqlite.py` の `JOBS` に入ってる文字列と一致)
- `match` は `both` か `job` か `name` が有効だが、`both` 強く推薦。
  - `job` はジョブでマッチするから匿名の戦闘記録を使う時に便利だけど、
    ジョブが同じだけど別人でも同じ人になってしまうから注意が必要
  - `name` は名前でマッチするから同じ人が複数ジョブ使ってるときに
    使う想定だったけど、データベース内で同じキャラに紐付けしてしまうと
   ステータスが合わなかったりして結局使い物にならなかったから強く非推薦
- `comment` はデータベース内にキャラ情報と一緒に保存されるだけだから
  同じキャラが複数回入ってる等してたらその見分けに使うとよい
  - 装備が違ってステータスが違うから同キャラの複数エントリーの場合等に有効
- `stats` はステータスをデータベースに保存するだけ

`reports` は例と同じようにデータを取得したい某戦闘記録サイトのレポートIDを
入れるだけ。

### 敵情報

データベースに敵の情報を書き込むために `collect-sqlite.py` 内の
`KNOWN_ENEMIES` に敵の情報を入れないといけない。

オメガ、竜詩、アルカディアライトヘビー級はすでに入ってるから追加する必要はない。

### データ取得実行

APIキー設定、データベース初期化、入力設定が完了したらデータ取得が実行できる。

`$ ./collect-sqlite.py -i input.yaml -o data.db`

このスクリプトはAPI制限等で途中で止められる想定で設計されてるから
`.collect-sqlite-progress.tmp` のファイルを削除しない限り停止しても再実行
すれば途中から再開してくれる。

データベースにすでに存在してる情報は上書きされない。なので複数回違う
入力ファイルで実行するのはあり。

#### ログインが必要な戦闘記録について

2年以上前や非公開等ログインが必要な戦闘記録は `-u` のフラグが
必要。

`-u` で実行してる場合、上記の
`'Press enter to open your browser to authenticate with FF Logs.'`
メッセージが出てきたらエンターを押してブラウザが開いて、
自己署名証明書を使ってるからセキュリティ警告を無視して、
某戦闘記録サイトにログインしたら使えるようになる。
なおセッションがものすごく短いことがあるから頻繁にセッションを更新しないと
行けない可能性がある。

### ポストプロセス

各技の威力や該当ジョブなど自動で取得できないデータが
あるから少し後処理が必要。`scripts-sql` に後処理ようの
sql のスクリプトが入ってる。

まずは各技のジョブ情報：

`$ sqlite3 data.db < scripts-sql/populate-ability-jobs.sql`

確定クリティカルと確定ダイレクトヒットの情報：

`$ sqlite3 data.db < scripts-sql/populate-guaranteed-cdh.sql`

次は各技の威力：

`$ sqlite3 data.db < scripts-sql/populate-potencies-7.05.sql`

`$ sqlite3 data.db < scripts-sql/populate-potencies-7.05-manual.sql`

（一行目のは自動（どこからかは忘れた）で取得したもので、二行目のは
dotや分身など特殊なスキルIDのものを手動で書いたもの）

最後は作中に紹介した標準威力：

`$ sqlite3 data.db < scripts-sql/populate-standardized-potencies.sql`

### スキーマ

これでデータ取得は完了。sql でデータを簡単にクエリーできる。

スキーマは `create-db.py` を確認するといい。
ドキュメンテーションこそ書いてないものの、カラムの名前でわかるように書いてあるはず。

`cards` と `card_buffs` のテーブルは使われてない。（じゃあ消せよ）

`sequence_infos`, `sequence_info_abilities`, `sequence_instances`,
`sequence_instance_events` のテーブルは別の `get-seq-2.py` と使える。
これは実際に実行されたスキルのシークエンスのダメージを集計するために使われてた
けど `get-seq-2.py` のスクリプト自体は暁月時代の絶オメガにロックされてるし
6.5から更新されてないしコンボやAAのせいで実際に全く一緒のスキルのシークエンス
が少なくてあんまり使い物にならなかった。実行する場合は複数回実行できないことだけ
注意してね（スクリプト内にも警告あるけど英語だから念の為）。

## その他のスクリプト

`standardized-potencies.R` は作中の線型モデルを作るのに使ったスクリプト。
その中に最も使えそうだけどちょっと複雑なクエリーがある。

`normal-test.R` はスキルのシークエンスの合計ダメージが正規分布なのを
アンダーソン・ダーリング検定で確認するスクリプト。

`old-scripts` には分析の初期に使ってたデータ取得と分析スクリプトが入ってる。
当時はデータベースではなく yaml ファイルに書いてたけど処理が
遅すぎたり使いづらすぎたりでボツにした。

# 連絡

コメントや質問はツイッター（@VSafili）かブルスカ（@vsafili.bsky.social）か
でリプかDMか、又はこのレポジトリ（ギットラボ）のイシューにどうぞ。
ギットハブは見てないから見えないぞ。

# ライセンス

このレポジトリ内のコードは全て GPLv2 に従って配布。

https://www.gnu.org/licenses/old-licenses/gpl-2.0.ja.html
